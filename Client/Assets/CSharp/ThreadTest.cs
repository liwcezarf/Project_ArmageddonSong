﻿using UnityEngine;
using System.Collections;
using System.Threading;

public class ThreadTest : MonoBehaviour
{
    private Camera _camera;

    private Thread _thread;

    //private bool _levelLoaded = false;


    void Start()
    {
        _camera = Camera.main;
        _thread = new Thread(Run);
        _thread.Start();
	}
	

    private void Run()
    {
        while(true)
        {
            Camera camera = _camera;
            camera.fieldOfView = 60;

            Debug.LogError("---");

            Thread.Sleep(100);

            //Application.LoadLevelAsync("Map_1001");
            //_levelLoaded = true;
        }
    }

    void OnApplicationQuit()
    {
        _thread.Abort();
    }
}
