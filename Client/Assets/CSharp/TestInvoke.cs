﻿using UnityEngine;
using System.Collections;

public class TestInvoke : MonoBehaviour
{
    private Animation _anim;


	// Use this for initialization
	void Start () {
        _anim = GetComponent<Animation>();
	}
	
	// Update is called once per frame
	//void Update ()
 //   {
	//    if(Input.GetMouseButtonDown(0))
 //       {
 //           Invoke("PlayAttack", 2f);
 //       }
	//}

    void OnGUI()
    {
        if(GUI.Button(new Rect(100, 100, 100, 30), "Play"))
        {
            InvokeRepeating("PlayAttack", 5f, 2f);
        }
    }


    private void PlayAttack()
    {
        _anim.Play("Attack1");
    }
}
