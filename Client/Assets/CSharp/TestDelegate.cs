﻿/*
 * 委托是一种在对象里保存方法引用的类型，也是一种类型安全的函数指针。
 * 事件是一种说是受限制的委托，只能施加+=,-=操作符。
 */

using UnityEngine;
using System.Collections;


public class TestDelegate : MonoBehaviour
{
    private PlayerInput _playerInput;

    private Animation _animation;

	// Use this for initialization
	void Start ()
    {
        _playerInput = new PlayerInput();
        _animation = GetComponent<Animation>();

        // 将方法绑定到委托
        _playerInput.MouseClickEvent = PlayAttack;
	}
	
	// Update is called once per frame
	void Update ()
    {
        _playerInput.Update();
	}

    private void PlayAttack()
    {
        _animation.Play("Attack1");
    }

    void OnDestroy()
    {
        // 解除绑定
        _playerInput.MouseClickEvent = PlayAttack;
    }
}

public class PlayerInput
{
    // 定义委托
    public delegate void MouseClick();

    public MouseClick MouseClickEvent;

    // 定义委托实例
    //public System.Action MouseClickEvent;

    // 定义事件
    //public event MouseClick MouseClickEvent;

   

    public void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            MouseClickEvent();
        }
    }
}

