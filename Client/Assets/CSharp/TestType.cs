﻿using UnityEngine;
using System;
using System.Reflection;
using Object = UnityEngine.Object;

public class Player
{
    private Transform _transform;

    public Player(string modelName)
    {
        // 获取角色模型路径
        string modelPath = "Units/" + modelName;

        // 载入角色模型并实例
        Object obj = Resources.Load<Object>(modelPath);
        _transform = (GameObject.Instantiate(obj) as GameObject).transform;
        _transform.position = Vector3.zero;
        _transform.localScale = Vector3.one;
    }
}

public class TestType : MonoBehaviour
{
	void Start ()
    {
        //1. 通过new创建对象
        Player p = new Player("C1001");


        Type t = typeof(Player);
        Type[] args = new Type[1];
        args[0] = typeof(string);

        // 构造函数的参数实例
        object[] argObjs = new object[1] { "C1001" };

        //2. 通过构造函数动态创建对象
        //// 获取构造器信息
        //ConstructorInfo constructorInfo = t.GetConstructor(args);

        //// 调用构造函数生成对象   
        //Player player = constructorInfo.Invoke(argObjs) as Player;


        //3. 通过Activator创建对象
        Player player = Activator.CreateInstance(t, argObjs) as Player;
    }
}