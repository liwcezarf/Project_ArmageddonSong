﻿using UnityEngine;
using System.Collections;
using System;
using System.Reflection;


public class MonsterA
{
    private Transform _transform;
    public MonsterA(string path)
    {
        UnityEngine.Object modelObj = Resources.Load(path);

        _transform = (GameObject.Instantiate(modelObj) as GameObject).transform;
        _transform.position = Vector3.zero;
        _transform.localScale = Vector3.one;
    }
}

public class ReflectionTest : MonoBehaviour
{
	void Start ()
    {
        // 1 通过new创建对象 
        //MonsterA monster = new MonsterA("Units/M1001");

        //Type t = typeof(MonsterA);

        // 通过类的名称获取类的类型
        Type t = Type.GetType("MonsterA");

        // 获取类的构造器的参数的类型
        Type[] args = new Type[1];
        args[0] = typeof(string);

        // 实例化参数列表中的元素
        System.Object[] argObjects = new System.Object[] { "Units/M1001" };


        // 2. 通过获取构造器信息创建动态对象
        //ConstructorInfo ci = t.GetConstructor(args);

        //MonsterA monster = ci.Invoke(argObjects) as MonsterA;

        // 3. 通过Activator动态创建对象
        MonsterA monster = Activator.CreateInstance(t, argObjects) as MonsterA;
    }
}
