﻿using UnityEngine;
using common;

/// <summary>
/// 关卡类型
/// </summary>
public enum UnityLevelType
{
    Startup = 0,
    Loading = 1,
    City = 2,
    Battle = 3
}

public class GameManager : MonoBehaviour
{

    void Awake()
    {
        DontDestroyOnLoad(this);

        /// 载入配置
        ConfigManager.instance.Initialize(new ConfigParser());

        LogManager log = gameObject.AddComponent<LogManager>();
        LogManager.Log("Log init");

        NetworkManager.instance.Initialize();

        WindowManager.instance.Initialize();
        WindowManager.instance.Open<LoginWnd>().Init();
    }

    void OnLevelWasLoaded(int levelID)
    {
        if (levelID == (int)UnityLevelType.Loading)
        {
            LoadingLevel.instance.Initialize();
        }
        else if (levelID == (int)UnityLevelType.City)
        {
            WindowManager.instance.Open<ActualCombatWnd>().Init();
        }
        else if (levelID >= (int)UnityLevelType.Battle)
        {
            LevelConfig config = ConfigManager.instance.GetLevelCfg((int)DataCache.instance.mapid);
            Battle.instance.Initialize(config);

            // 如果消息缓存中有出生的玩家数据，则创建玩家
            for (int i = 0; i < DataCache.instance.roles.Count; i++)
            {
                RoleData roledata = DataCache.instance.roles[i];

                if (roledata.accountName == DataCache.instance.accountName)
                    roledata.RoleType = RoleType.MainHero;

                Battle.instance.CreateRole(roledata);
            }
            DataCache.instance.roles.Clear();
        }
    }

    void Update()
    {
        // 每一帧的增量时间
        float dt = Time.deltaTime;

        TimerManager.instance.Tick(dt);
        NetworkManager.instance.Update();
        WindowManager.instance.Update(dt);
        Battle.instance.Update(dt);
    }

    void OnApplicationQuit()
    {
        NetworkManager.instance.Disconnect();
    }
}


