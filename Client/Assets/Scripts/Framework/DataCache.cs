﻿using System;
using System.Collections.Generic;

using common;
using server;

public class DataCache : Singleton<DataCache>
{

    // 账户名
    public string accountName;

    // 选择的英雄ID
    public int myHeroID;

    // 战斗中的所有角色数据
    public List<RoleData> roles = new List<RoleData>();

    // 地图id
    public uint mapid;
}