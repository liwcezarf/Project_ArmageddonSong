﻿
using System.Collections.Generic;
using UnityEngine;
using System.Linq;



public class WindowManager : Singleton<WindowManager>
{
    private Dictionary<string, BaseWindow> _windows = new Dictionary<string, BaseWindow>();

    private Transform _canvas;
    public Camera uiCamera;
    public void Initialize()
    {
        
        Transform uiTran = GameObject.Find("UI").transform;
        uiTran.gameObject.AddComponent<DontDestroyOnLoad>();

        _canvas = uiTran.FindChild("Canvas");

        uiCamera = GameObject.Find("UI/Camera").GetComponent<Camera>();
    }

    /// <summary>
    /// 打开界面
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public T Open<T>() where T : BaseWindow, new()
    {
        string wndName = typeof(T).Name;
        if (_windows.ContainsKey(wndName))
        {
            return _windows[wndName] as T;
        }
        else
        {
            T wnd = new T();
            wnd.InitWnd(wndName, _canvas);
            _windows.Add(wndName, wnd);

            return wnd;
        }
    }

    /// <summary>
    /// 关闭界面
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public void Close<T>() where T : BaseWindow
    {
        string wndName = typeof(T).Name;
        if (_windows.ContainsKey(wndName))
        {
            T wnd = _windows[wndName] as T;
            wnd.Close();
            _windows.Remove(wndName);
        }
    }

    /// <summary>
    /// 获取界面
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public T GetWnd<T>() where T : BaseWindow
    {
        string wndName = typeof(T).Name;
        if (_windows.ContainsKey(wndName))
        {
            return _windows[wndName] as T;
        }
        else
        {
            return null;
        }
    }

    public void CloseAll()
    {
        foreach (BaseWindow wnd in _windows.Values.ToArray())
        {
            wnd.Close();

            if (_windows.ContainsKey(wnd.name))
            {
                _windows.Remove(wnd.name);
            }
        }
    }

    public void Update(float dt)
    {
        foreach (BaseWindow wnd in _windows.Values)
        {
            wnd.Update(dt);
        }
    }
}

