﻿using System;
using System.Collections.Generic;
using System.Linq;



public class Timer
{
    private float _elapsedTime;

    private Action _action;

    private Action _cancelAction;

    private float _delay;

    private bool _end = false;

    private bool _repeat = false;

    public bool end
    {
        get
        {
            return _end;
        }
    }

    public int globalID;

    public static void Invoke(Action action, Action cancelAction, float delay)
    {
        Timer timer = new Timer(action, cancelAction, delay);
        TimerManager.instance.AddTimer(timer);
    }


    public static void InvokeRepeat(Action action, Action cancelAction, float delay)
    {
        Timer timer = new Timer(action, cancelAction, delay, true);
        TimerManager.instance.AddTimer(timer);
    }

    private Timer(Action action, Action cancelAction, float delay, bool repeat = false)
    {
        _action = action;
        _delay = delay;

        _repeat = repeat;

        _cancelAction = cancelAction;

        _cancelAction = OnCancel;
    }

    private void OnCancel()
    {
        _end = true;
    }
    public void Tick(float dt)
    {
        if (_elapsedTime >= _delay)
        {
            if (_repeat)
            {
                _elapsedTime = _elapsedTime - _delay;
            }
            else
            {
                _end = true;
            }

            if (_action != null)
                _action();
        }

        _elapsedTime += dt;
    }
}


public class TimerManager : Singleton<TimerManager>
{
    private Dictionary<int, Timer> _timers = new Dictionary<int, Timer>();

    private int timerID = 0;

    public void AddTimer(Timer timer)
    {
        timer.globalID = timerID;
        _timers.Add(timer.globalID, timer);
        timerID++;
    }

    public void Tick(float dt)
    {
        foreach (Timer timer in _timers.Values.ToArray())
        {
            if (timer.end)
            {
                _timers.Remove(timer.globalID);
            }
            else
            {
                timer.Tick(dt);
            }
        }
    }
}