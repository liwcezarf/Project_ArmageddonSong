﻿using System;
using System.Collections.Generic;
using UnityEngine;
using client;


public class BattleSyncSender
{
    public static void RoleMove(Vector3 dest)
    {
        ClientMsg req = new ClientMsg();
        req.RoleMove = new client.ReqMove();
        req.RoleMove.dest = ProtoHelper.UV2PV(dest);

        NetworkManager.instance.Send((int)MsgID.ReqMove, req);
    }


    public static void ReqSyncHeroPos(Vector3 dest, Vector3 farward)
    {
        ClientMsg req = new ClientMsg();
        req.syncHeroPos = new ReqSyncHeroPos();
        req.syncHeroPos.dest = ProtoHelper.UV2PV(dest);
        req.syncHeroPos.forward = ProtoHelper.UV2PV(farward);

        NetworkManager.instance.Send((int)MsgID.ReqSyncHeroPos, req);
    }

    public static void RoleAttack(uint skillid, uint targetid)
    {
        ClientMsg req = new ClientMsg();
        req.RoleAttack = new client.ReqAttack();
        req.RoleAttack.skillID = skillid;
        req.RoleAttack.targetID = targetid;
        NetworkManager.instance.Send((int)MsgID.ReqAttack, req);
    }
}