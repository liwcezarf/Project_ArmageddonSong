﻿using System;
using server;
using common;
using System.Collections.Generic;

public class BattleHandler : IMsgHandler
{
    public void RegisterMsg(Dictionary<MsgID, Action<SocketModel>> handlers)
    {
        handlers.Add(MsgID.RespRoleBorn, OnRoleBorn);
        handlers.Add(MsgID.NotifyRoleDie, OnRoleDie);
        handlers.Add(MsgID.NotifyRoleAttack, OnRoleAttack);
        handlers.Add(MsgID.NotifyRoleMove, OnRoleMove);
        handlers.Add(MsgID.NotifyRoleIdle, OnRoleIdle);
        handlers.Add(MsgID.NotifyHPChange, OnNotifyHpChange);
        handlers.Add(MsgID.NotifySyncHeroPos, OnSyncHeroPos);
    }

    private  void OnRoleBorn(SocketModel model)
    {
        ServerMsg resp = SerializeUtil.Deserialize<ServerMsg>(model.message);

        for (int i = 0; i < resp.RoleBorn.RoleList.Count; i++)
        {
            RoleData roledata = resp.RoleBorn.RoleList[i];

            if (roledata.accountName == DataCache.instance.accountName)
                roledata.RoleType = RoleType.MainHero;

            Battle.instance.CreateRole(roledata);
        }
    }

    /// <summary>
    /// 角色死亡应答
    /// </summary>
    /// <param name="data"></param>
    private  void OnRoleDie(SocketModel model)
    {
        ServerMsg notify = SerializeUtil.Deserialize<ServerMsg>(model.message);

        Character role = Battle.instance.GetRole(notify.RoleDie.globalRoleID);
        if(role != null)
            role.Die();
    }

    /// <summary>
    /// 角色攻击应答
    /// </summary>
    /// <param name="data"></param>
    private  void OnRoleAttack(SocketModel model)
    {
        ServerMsg notify = SerializeUtil.Deserialize<ServerMsg>(model.message);

        Character role = Battle.instance.GetRole(notify.RoleAttack.globalRoleID);


        role.position = ProtoHelper.PV2UV(notify.RoleAttack.pos);
        role.Attack((int)notify.RoleAttack.skillID, notify.RoleAttack.targetid);
    }

    /// <summary>
    /// 角色掉血应答
    /// </summary>
    /// <param name="data"></param>
    private  void OnNotifyHpChange(SocketModel model)
    {
        ServerMsg notify = SerializeUtil.Deserialize<ServerMsg>(model.message);

        Character role = Battle.instance.GetRole(notify.HPChange.targetid);

        int hp = Math.Abs(notify.HPChange.hp);
        role.Wound(hp);
    }

    /// <summary>
    /// 角色移动应答
    /// </summary>
    /// <param name="data"></param>
    private  void OnRoleMove(SocketModel model)
    {
        ServerMsg notify = SerializeUtil.Deserialize<ServerMsg>(model.message);

        Character role = Battle.instance.GetRole(notify.RoleMove.globalRoleID);
        role.Move(ProtoHelper.PV2UV(notify.RoleMove.position));
    }

    /// <summary>
    /// 角色移动应答
    /// </summary>
    /// <param name="data"></param>
    private void OnSyncHeroPos(SocketModel model)
    {
        ServerMsg notify = SerializeUtil.Deserialize<ServerMsg>(model.message);

        LogManager.Log(notify.syncHeroPos.position);
        Character role = Battle.instance.GetRole(notify.syncHeroPos.globalRoleID);
        role.SyncHeroPos(ProtoHelper.PV2UV(notify.syncHeroPos.position), ProtoHelper.PV2UV(notify.syncHeroPos.forward));
    }

    /// <summary>
    /// 角色停止移动
    /// </summary>
    /// <param name="data"></param>
    private void OnRoleIdle(SocketModel model)
    {
        ServerMsg notify = SerializeUtil.Deserialize<ServerMsg>(model.message);

        Character role = Battle.instance.GetRole(notify.RoleIdle.globalRoleID);

        role.Idle();
    }
}