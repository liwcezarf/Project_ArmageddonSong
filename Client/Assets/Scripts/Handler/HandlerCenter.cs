﻿using System;
using System.Collections.Generic;

using UnityEngine;

public interface IMsgHandler
{
    void RegisterMsg(Dictionary<MsgID, Action<SocketModel>> handlers);
}


public class HandlerCenter : IHandlerCenter
{
    private Dictionary<MsgID, Action<SocketModel>> _handlers = new Dictionary<MsgID, Action<SocketModel>>();

    private AccountHandler _loginHandler = new AccountHandler();

    private MatchHandler _worldHandler = new MatchHandler();

    private BattleHandler _levelHandler = new BattleHandler();

    public void Initialize()
    {
        _loginHandler.RegisterMsg(_handlers);
        _worldHandler.RegisterMsg(_handlers);
        _levelHandler.RegisterMsg(_handlers);
    }

    public void MessageReceive(SocketModel model)
    {
        Action<SocketModel> handler = _handlers[(MsgID)model.command];
        handler(model);
    }
}