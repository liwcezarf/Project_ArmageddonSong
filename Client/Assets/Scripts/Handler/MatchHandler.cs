﻿using System;
using System.Collections.Generic;

using server;

public class MatchHandler : IMsgHandler
{
    public void RegisterMsg(Dictionary<MsgID, Action<SocketModel>> handlers)
    {
        handlers.Add(MsgID.RespPraticeLevel, OnPraticeLevel);
        handlers.Add(MsgID.RespActualCombat, OnActualCombat);
        handlers.Add(MsgID.RespCancelMatch, OnCancelMatch);

        handlers.Add(MsgID.NotifyChooseHero, OnNotifyChooseHero);
        handlers.Add(MsgID.NotifyReady, OnNotifyReady);
        handlers.Add(MsgID.NotifyEnterLevel, OnNotifyEnterLevel);
        handlers.Add(MsgID.NotifyExitLevel, OnExitLevel);
    }

    private void OnPraticeLevel(SocketModel model)
    {
        ServerMsg resp = SerializeUtil.Deserialize<ServerMsg>(model.message);
        if (resp.ErrorCode != 0)
        {
            MessageBox.Show(Enum.GetName(typeof(ErrorCode), resp.ErrorCode));
        }
        else
        {
            WindowManager.instance.Open<MatchWnd>().Init(common.LevelType.Pratice);
        }

    }

    private void OnActualCombat(SocketModel model)
    {
        ServerMsg resp = SerializeUtil.Deserialize<ServerMsg>(model.message);
        if (resp.ErrorCode != 0)
        {
            MessageBox.Show(Enum.GetName(typeof(ErrorCode), resp.ErrorCode));
        }
        else
        {
            WindowManager.instance.Open<MatchWnd>().Init(common.LevelType.ActualCombat);
        }
    }

    private void OnCancelMatch(SocketModel model)
    {
        ServerMsg resp = SerializeUtil.Deserialize<ServerMsg>(model.message);
        if (resp.ErrorCode != 0)
        {
            MessageBox.Show(Enum.GetName(typeof(ErrorCode), resp.ErrorCode));
        }
        else
        {
            WindowManager.instance.Close<MatchWnd>();
            WindowManager.instance.Close<ChooseHeroWnd>();
        }
    }

    private void OnNotifyChooseHero(SocketModel model)
    {
        ServerMsg resp = SerializeUtil.Deserialize<ServerMsg>(model.message);
        if (resp.ErrorCode != 0)
        {
            MessageBox.Show(Enum.GetName(typeof(ErrorCode), resp.ErrorCode));
        }
        else
        {
            WindowManager.instance.Close<MatchWnd>();
            WindowManager.instance.Open<ChooseHeroWnd>().Init(resp.ChooseHero);
        }
    }

    private void OnNotifyReady(SocketModel model)
    {
        ServerMsg notify = SerializeUtil.Deserialize<ServerMsg>(model.message);
        if (notify.ErrorCode != 0)
        {
            MessageBox.Show(Enum.GetName(typeof(ErrorCode), notify.ErrorCode));
        }
        else
        {
            if (notify.Ready.accountName == DataCache.instance.accountName)
            {
                DataCache.instance.myHeroID = (int)notify.Ready.heroid;
                WindowManager.instance.GetWnd<ChooseHeroWnd>().OnAccountReady(notify.Ready);
            }
            else
            {
                WindowManager.instance.GetWnd<ChooseHeroWnd>().OnUpdateSelect(notify.Ready.side, notify.Ready.index, notify.Ready.heroid);
            }
        }
    }

    private void OnNotifyEnterLevel(SocketModel model)
    {
        ServerMsg notify = SerializeUtil.Deserialize<ServerMsg>(model.message);
        if (notify.ErrorCode != 0)
        {
            MessageBox.Show(Enum.GetName(typeof(ErrorCode), notify.ErrorCode));
        }
        else
        {
            LevelConfig levelCfg = ConfigManager.instance.GetLevelCfg((int)notify.EnterLevel.mapid);

            // 将角色出生数据添加到消息缓存
            for (int i = 0; i < notify.EnterLevel.RoleList.Count; i++)
            {
                DataCache.instance.roles.Add(notify.EnterLevel.RoleList[i]);
            }
            
            DataCache.instance.mapid = notify.EnterLevel.mapid;

            LoadingLevel.instance.LoadScene(levelCfg.Scene);
        }
    }

    private void OnExitLevel(SocketModel model)
    {
        ServerMsg notify = SerializeUtil.Deserialize<ServerMsg>(model.message);
    }
}