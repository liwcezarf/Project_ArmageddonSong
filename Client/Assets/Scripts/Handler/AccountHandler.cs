﻿using System;
using System.Collections.Generic;

using server;

public class AccountHandler : IMsgHandler
{
    public void RegisterMsg(Dictionary<MsgID, Action<SocketModel>> handlers)
    {
        handlers.Add(MsgID.RespRegist, OnRegist);
        handlers.Add(MsgID.RespLogin, OnLogin);
    }

    /// <summary>
    /// 注册
    /// </summary>
    /// <param name="data"></param>
    /// <param name="user"></param>
    private  void OnRegist(SocketModel model)
    {
        ServerMsg resp = SerializeUtil.Deserialize<ServerMsg>(model.message);
        if (resp.ErrorCode == 0)
        {
            MessageBox.Show("Register Success");
        }
        else
        {
            string error = Enum.GetName(typeof(ErrorCode), resp.ErrorCode);
            MessageBox.Show(error);
        }
    }

    /// <summary>
    /// 登录
    /// </summary>
    /// <param name="data"></param>
    /// <param name="user"></param>
    private  void OnLogin(SocketModel model)
    {
        ServerMsg resp = SerializeUtil.Deserialize<ServerMsg>(model.message);

        if (resp.ErrorCode == 0)
        {
            DataCache.instance.accountName = resp.Login.Account;

            WindowManager.instance.Close<LoginWnd>();

            //LoadingLevel.levelName = "Main";
            //UnityEngine.SceneManagement.SceneManager.LoadScene("Loading");            
            LoadingLevel.instance.LoadScene("Main");
        }
        else
        {
            string error = Enum.GetName(typeof(ErrorCode), resp.ErrorCode);
            MessageBox.Show(error);
        }
    }
}