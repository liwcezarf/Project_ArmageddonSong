﻿using System;
using System.Collections.Generic;
using System.Text;
using common;

/// <summary>
/// 技能组配置
/// </summary>
public class SkillGroupCfg
{
    public int ID;
    public int RoleID;
    public int Index;
}

/// <summary>
/// 法术配置
/// </summary>
public class SkillSpellCfg
{
    // Basic
    public int ID;

    public string Name;
    public string Desc;
    public int BasicNum;
    public string AnimName;
    public float CD;
    public float HitTime;
    public int HitNum;                  // 攻击次数
    public float HitInterval;           // 连击的攻击间隔时间

    // Skill 
    public SkillType SkillType;
    public float MaxRange;
    public AffectSide AffectSide;
    public IgnoreType IgnoreTarget;

    // Aoe
    public AreaType AreaCenter;
    public AreaShape AreaShape;
    public float AreaArg1;
    public float AreaArg2;

    // 特效
    public string CastEffect;           // 释放特效
    public string HitEffect;            // 命中特效
    public string PosEffect;            // 定点特效
    public string BulletEffect;         // 子弹特效
}

/// <summary>
/// 子弹配置
/// </summary>
public class SkillBulletCfg
{
    public int ID;
    public BulletType BulletType;
    public int CastNum;
    public float SplitAngle;
    public float FlyRange;              // 最大飞行距离
    public byte FlyPierce;              // 可穿透
    public float FlySpeedH;             // 水平速度
    public float FlySpeedV;             // 垂直速度
    public byte FlyTrack;               // 是否跟踪
    public int FlyBounceNum;            // 弹射次数

    public string BulletEffect;         // 子弹特效
}

partial class ConfigManager
{
    /// <summary>
    /// 技能组
    /// </summary>
    private Dictionary<int, SkillGroupCfg> _skillGroupConfigs = new Dictionary<int, SkillGroupCfg>();

    /// <summary>
    /// 法术
    /// </summary>
    private Dictionary<int, SkillSpellCfg> _skillSpellConfigs = new Dictionary<int, SkillSpellCfg>();

    /// <summary>
    /// 子弹
    /// </summary>
    private Dictionary<int, SkillBulletCfg> _skillBulletConfigs = new Dictionary<int, SkillBulletCfg>();


    /// <summary>
    /// 通过角色ID获取该角色拥有的所有技能
    /// </summary>
    /// <param name="roleID"></param>
    /// <returns></returns>
    public Dictionary<int, SkillGroupCfg> GetSkillGroup(int roleID)
    {
        Dictionary<int, SkillGroupCfg> skillGroups = new Dictionary<int, SkillGroupCfg>();
        foreach (SkillGroupCfg skillGroupCfg in _skillGroupConfigs.Values)
        {
            if (skillGroupCfg.RoleID == roleID)
            {
                skillGroups.Add(skillGroupCfg.ID, skillGroupCfg);
            }
        }
        return skillGroups;
    }

    /// <summary>
    /// 通过技能ID获取法术配置
    /// </summary>
    /// <param name="skillID"></param>
    /// <returns></returns>
    public SkillSpellCfg GetSkillSpell(int skillID)
    {
        if (_skillSpellConfigs.ContainsKey(skillID))
            return _skillSpellConfigs[skillID];
        return null;
    }

    /// <summary>
    /// 获取子弹配置
    /// </summary>
    /// <param name="skillID"></param>
    /// <returns></returns>
    public SkillBulletCfg GetBullet(int skillID)
    {
        if (_skillBulletConfigs.ContainsKey(skillID))
        {
            return _skillBulletConfigs[skillID];
        }
        return null;
    }

    /// <summary>
    /// 通过角色ID和技能索引获取技能ID
    /// </summary>
    /// <param name="roleID"></param>
    /// <param name="skillIndex"></param>
    /// <returns></returns>
    public int GetSkillID(int roleID, int skillIndex)
    {
        foreach (SkillGroupCfg cfg in _skillGroupConfigs.Values)
        {
            if (cfg.RoleID == roleID && cfg.Index == skillIndex)
            {
                return cfg.ID;
            }
        }

        return 0;
    }
}