﻿using common;
using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// 关卡配置
/// </summary>
public class LevelConfig
{
    public int ID;
    public string Name;
    public LevelType Type;
    public int PlayerNumber;
    public string Scene;
    public string Map;
}

/// <summary>
/// 出生点配置
/// </summary>
public class BornPointCfg
{
    public int ID;
    public RoleType RoleType;

    public RoleSide Side;
    public int LevelID;
    public string InitPosition;
    public string InitRotation;
}

partial class ConfigManager
{
    /// <summary>
    /// 关卡配置
    /// </summary>
    public Dictionary<int, LevelConfig> levelConfigs = new Dictionary<int, LevelConfig>();

    /// <summary>
    /// 出生点配置
    /// </summary>
    private Dictionary<int, BornPointCfg> _bornPointConfigs = new Dictionary<int, BornPointCfg>();

    
    /// <summary>
    /// 通过关卡ID获取关卡配置
    /// </summary>
    /// <param name="levelID"></param>
    /// <returns></returns>
    public LevelConfig GetLevelCfg(int levelID)
    {
        if (levelConfigs.ContainsKey(levelID))
        {
            return levelConfigs[levelID];
        }
        return null;
    }


    public LevelConfig GetLevel(common.LevelType type, int playerNumber)
    {
        foreach (LevelConfig cfg in levelConfigs.Values)
        {
            if (cfg.Type == type && cfg.PlayerNumber == playerNumber)
            {
                return cfg;
            }
        }

        return null;
    }
    public LevelConfig GetLevel(int ID)
    {
        if (levelConfigs.ContainsKey(ID))
        {
            return levelConfigs[ID];
        }
        else
        {
            return null;
        }
    }

    public BornPointCfg GetBornPoint(int levelID, common.RoleType type, RoleSide side)
    {
        foreach (BornPointCfg cfg in _bornPointConfigs.Values)
        {
            if (cfg.LevelID == levelID && cfg.RoleType == type && cfg.Side == side)
            {
                return cfg;
            }
        }
        return null;
    }

    public Dictionary<int, BornPointCfg> GetBornPointCfgs(int levelID)
    {
        Dictionary<int, BornPointCfg> bornPointConfigs = new Dictionary<int, BornPointCfg>();
        foreach (BornPointCfg cfg in _bornPointConfigs.Values)
        {
            if (cfg.LevelID == levelID && !bornPointConfigs.ContainsKey(levelID))
            {
                bornPointConfigs.Add(levelID, cfg);
            }
        }

        return bornPointConfigs;
    }

}