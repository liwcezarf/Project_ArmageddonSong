﻿using System;
using System.Collections.Generic;


public class Singleton<T> where T : class, new()
{
    private static T _instance;

    private static readonly object _locker = new object();

    public Singleton()
    {

    }

    public static T instance
    {

        get
        {
            lock (_locker)
            {
                if (_instance == null)
                {
                    _instance = new T();
                }
                return _instance;
            }
        }
    }
}