﻿using common;
using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// 角色配置
/// </summary>
public class RoleConfig
{
    public int ID;
    public string ModelName;
    public string HeadPic;
    public RoleType RoleType;
    public int Hp;
    public int Mp;
    public int Sight;       // 视野
    public int AttackRange; // 攻击范围
    public string Behavior; // 决策
}

partial class ConfigManager
{
    /// <summary>
    /// 角色配置字典
    /// </summary>
    private Dictionary<int, RoleConfig> _roleConfigs = new Dictionary<int, RoleConfig>();

    /// <summary>
    /// 通过角色ID获取角色配置
    /// </summary>
    /// <param name="ID"></param>
    /// <returns></returns>
    public RoleConfig GetRoleConfig(int ID)
    {
        if (_roleConfigs.ContainsKey(ID))
            return _roleConfigs[ID];

        return null;
    }

    /// <summary>
    /// 通过角色类型获取角色配置中的所有角色配置信息
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public Dictionary<int, RoleConfig> GetRolesByType(common.RoleType type)
    {
        Dictionary<int, RoleConfig> roleConfigs = new Dictionary<int, RoleConfig>();
        foreach (RoleConfig cfg in _roleConfigs.Values)
        {
            if (cfg.RoleType == type && !roleConfigs.ContainsKey(cfg.ID))
            {
                roleConfigs.Add(cfg.ID, cfg);
            }
        }

        return roleConfigs;
    }

}