﻿
using common;
using UnityEngine;
using UnityEngine.EventSystems;
using Vector3 = UnityEngine.Vector3;


public class MainHero : Mover
{
    private ETCJoystick _joystick;

    // 选中目标时，角色脚下的标记
    private Transform _targetTip;

    // 主相机，跟随相机
    private Camera _camera;

    // 相机偏移
    private Vector3 _cameraOffset;

    private const float _fixedSyncTime = 0.05f;

    private float _elapsedSyncTime = 0;

    // 是否正在移动
    private bool _moving = false;

    public MainHero(LunaNavmeshQuery navmesh, RoleConfig config, RoleData data) : base(navmesh, config, data)
    {
        // 获取虚拟摇杆，添加摇杆监听事件
        Transform easyTouchCanvas = GameObject.Find("EasyTouch").transform;
        _joystick = easyTouchCanvas.FindChild("Joystick").GetComponent<ETCJoystick>();
        _joystick.gameObject.SetActive(true);
        _joystick.onMove.AddListener(OnJoystickMove);
        _joystick.onMoveStart.AddListener(OnJoystickMoveStart);
        _joystick.onMoveEnd.AddListener(OnJoystickMoveEnd);

        _camera = Camera.main;

        if (data.RoleSide == RoleSide.Red)
            _camera.transform.position += new Vector3(160, 0, 0);

        // 获取相机偏移
        _cameraOffset = _camera.transform.position - position;
    }

    private void OnJoystickMove(Vector2 vec)
    {
        Vector3 delta = new Vector3(vec.x, 0, vec.y) * 3;
        //Vector3 pos = _initAttibute.roleSide == RoleSide.Blue ? position + delta : position -delta;
        Vector3 pos = position + delta;
        Move(pos);

        //LevelMsgSender.RoleMove(pos);
    }

    private void OnJoystickMoveStart()
    {
        _moving = true;
    }

    private void OnJoystickMoveEnd()
    {
        _moving = false;
    }

    public override void Update(float dt)
    {
        base.Update(dt);
        // 更新鼠标输入
        UpdateMouseInput();

        // 更新键盘输入
        UpdateKeyboardInput();

        SyncHeroPos(dt);

        // 更新相机跟随
        UpdateCamera();
    }

    public void SyncHeroPos(float dt)
    {
        if (!_moving) return;
        if (_elapsedSyncTime > _fixedSyncTime)
        {
            BattleSyncSender.ReqSyncHeroPos(position, forward);
            _elapsedSyncTime = _elapsedSyncTime - _fixedSyncTime;
        }

        _elapsedSyncTime += dt;
    }

    public override void Idle()
    {
        base.Idle();
        BattleSyncSender.ReqSyncHeroPos(position, forward);
    }

    /// <summary>
    /// 更新玩家输入
    /// </summary>
    private void UpdateMouseInput()
    {
        if (Input.GetMouseButtonDown(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))
        {
#if IPHONE || ANDROID
			if (!EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
#else
            if (!EventSystem.current.IsPointerOverGameObject())
#endif
            { 
                // 通过屏幕上的一点从相机发射一条射线
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                // 定义射线投射碰撞点信息
                RaycastHit hitInfo;

                int roleLayer = 1 << LayerMask.NameToLayer("Role");
                int terrainLayer = 1 << LayerMask.NameToLayer("Terrain");
                int triggerLayer = 1 << LayerMask.NameToLayer("BornTrigger");

                // 通过射线原点和射线朝向计算碰撞点的信息
                if (Physics.Raycast(ray.origin, ray.direction, out hitInfo))
                {
                    // 获取射线碰撞的碰撞体的游戏对象的层
                    int layer = hitInfo.collider.gameObject.layer;

                    if (layer == LayerMask.NameToLayer("Role"))
                    {
                        IDAgent idAgent = hitInfo.collider.gameObject.GetComponent<IDAgent>();

                        Character role =  Battle.instance.GetRole((uint)idAgent.globalID);
                        if (role != null)
                        {
                            _targetTip.position = role.position;
                            _targetTip.parent = role.transform;

                            // 设置玩家的当前目标
                            target = role;
                        }
                    }
                    else
                    {
                        // 玩家移动到地形上的射线碰撞到的一点
                        Vector3 realDest = new Vector3(hitInfo.point.x, position.y, hitInfo.point.z);
                        BattleSyncSender.RoleMove(realDest);
                    }
                }
            }
        }
    }

    /// <summary>
    /// 更新键盘输入
    /// </summary>
    private void UpdateKeyboardInput()
    {
        if (Input.GetKeyUp(KeyCode.Alpha1))
        {
            PlayerAttack(1);
        }
        else if (Input.GetKeyUp(KeyCode.Alpha2))
        {
            PlayerAttack(2);
        }
    }

    /// <summary>
    /// 更新相机跟随
    /// </summary>
    private void UpdateCamera()
    {
       _camera.transform.position = position + _cameraOffset;
    }

    /// <summary>
    /// 玩家攻击
    /// </summary>
    /// <param name="index"></param>
    private void PlayerAttack(int index)
    {
        if (target == null)
            return;

        Chase(target, index);
    }

    public override void Die()
    {
        base.Die();
        _joystick.onMove.RemoveListener(OnJoystickMove);
        _joystick.gameObject.SetActive(false);
    }
}

