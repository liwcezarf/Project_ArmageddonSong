﻿
using System.Collections.Generic;
using common;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class Behavior : SkillContainer
{
    public Behavior(LunaNavmeshQuery navmesh, RoleConfig config, RoleData data) : base(navmesh, config, data)
    {
    }


    /// <summary>
    /// 获取敌方某种类型的单位
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public override Dictionary<int, Character> GetTypeEnemies(RoleType type)
    {
        RoleSide enemySide = RoleSide.Neutrality;
        if (side == RoleSide.Blue)
        {
            enemySide = RoleSide.Red;
        }
        else if (side == RoleSide.Red)
        {
            enemySide = RoleSide.Blue;
        }

        return Battle.instance.GetSideTypeRoles(enemySide, type);
    }

    /// <summary>
    /// 获取视野内的最近敌人
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public override Character GetNearestEnemyInSight(RoleType type)
    {
        Character enemy = null;
        Dictionary<int, Character> roles = GetTypeEnemies(type);

        float neareastDistance = Mathf.Infinity;
        foreach (Character role in roles.Values)
        {
            float distance = Vector3.Distance(position, role.position);
            if (distance <= neareastDistance && distance <= _initAttibute.sight)
            {
                enemy = role;
                neareastDistance = distance;
            }
        }

        return enemy;
    }
}

