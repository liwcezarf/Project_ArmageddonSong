﻿using common;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class Mover : Behavior
{   
    // 导航代理
    private LunaNavAgent _navAgent;

    // 目标点
    private Vector3 _destination;

    public Mover(LunaNavmeshQuery navmesh, RoleConfig config, RoleData data) : base(navmesh, config, data)
    {
        // 初始化导航代理
        _navAgent = new LunaNavAgent();
        _navAgent.Init(navmesh, _transform, Idle, Move);
    }


    public override void Move(Vector3 dest)
    {
        base.Move(dest);

        Vector3 origin = _transform.position + new Vector3(0, 0.8f, 0);
        Vector3 dir = (dest - _transform.position).normalized;

        int layer = 1 << LayerMask.NameToLayer("Door");
        if (Physics.Raycast(origin, dir, 1f, layer))
            return;

        // 若果导航代理处于关闭状态，则开启
        if (!_navAgent.enabled)
            _navAgent.enabled = true;

        // 导航代理移动到目标点
        _navAgent.Move(dest);

        _destination = dest;
    }

    public void Move()
    {
        Vector3 origin = _transform.position + new Vector3(0, 0.8f, 0);
        Vector3 dir = _transform.forward;
        Vector3 goalDir = (_destination - _transform.position).normalized;

        int layer = 1 << LayerMask.NameToLayer("Door");
        if (Physics.Raycast(origin, dir, 1f, layer) && Physics.Raycast(origin, goalDir, 1f, layer))
        {
            Idle();
        }
    }

    public override void Idle()
    {
        base.Idle();

        // 若果导航代理处于开启状态，则关闭
        if (_navAgent.enabled)
            _navAgent.enabled = false;
    }

    public override void Update(float dt)
    {
        base.Update(dt);

        // 更新导航代理
        _navAgent.Update(dt);
    }
}

