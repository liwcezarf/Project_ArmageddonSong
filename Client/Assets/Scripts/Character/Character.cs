﻿
using UnityEngine;
using System.Collections.Generic;
using common;
using Vector3 = UnityEngine.Vector3;
using System;

public class IDAgent : MonoBehaviour
{
    public int globalID;
}

public abstract class Character
{
    /// <summary>
    /// 全局唯一ID
    /// </summary>
    private int _globalID;

    // 角色类型
    private RoleType _type;

    // 角色动画
    protected Animation _animation;

    // 游戏对象的变换数据
    protected Transform _transform;

    
    // 角色初始属性
    protected CharacterAttribute _initAttibute;

    // 角色实时属性
    protected CharacterAttribute _attribute;

    // 角色碰撞器
    private CapsuleCollider _collider;

    // 全局唯一ID的代理
    private IDAgent _idAgent;

    // 目标
    public Character target;

    // 血量改变
    public Action<Character, int, BloodTextType> hpChanged;

    // 角色死亡事件
    public Action<Character> characterDie;


    // 角色的当前坐标
    public Vector3 position
    {
        get
        {
            return _transform.position;
        }
        set
        {
            _transform.position = value;
        }
    }

    public RoleType type { get { return _type; } }

    public Transform transform { get { return _transform; } }

    public int globalID { get { return _globalID; } }

    public float attackRange { get { return _initAttibute.attackRange; } }

    public int hp { get { return _attribute.hp; } }

    public int totalhp { get { return _initAttibute.hp; } }

    // 角色是否活着
    public bool alive { get { return _attribute.hp > 0; } }

    // 角色阵营
    public RoleSide side { get { return _attribute.roleSide; } }

    // 角色朝向
    public Vector3 forward { get { return _transform.forward; } }

    public int roleID { get { return _initAttibute.roleID; } }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="globalID">全局ID</param>
    /// <param name="roleConfig">角色配置</param>
    /// <param name="pos">出生点</param>
    /// <param name="rot">朝向</param>
    public Character(LunaNavmeshQuery navmesh, RoleConfig config, RoleData data)
    {
        _globalID = (int)data.UniID;
        LogManager.Log("Role Global ID:" + _globalID);

        // 初始化角色初始属性和实时属性
        _initAttibute = CharacterAttribute.GetAttribute(config, data.RoleSide);
        _attribute = _initAttibute.Clone();

        _type = _initAttibute.roleType;

        _transform = (PoolManager.instance.Spawn("Units/", config.ModelName)).transform;

        // 设置角色的初始坐标、缩放
        Vector3 pos = ProtoHelper.PV2UV(data.Pos);
        Vector3 rot = ProtoHelper.PV2UV(data.Dir);
        _transform.position = pos;
        _transform.localScale = Vector3.one;

        // 获取角色的动画组件
        _animation = _transform.GetComponent<Animation>();

        // 获取或者添加碰撞器并初始化参数
        _collider = _transform.GetComponent<CapsuleCollider>();
        if (_collider == null)
            _collider = _transform.gameObject.AddComponent<CapsuleCollider>();
        _collider.height = 2.0f;
        _collider.center = new Vector3(0, 0.9f, 0);

        _idAgent = _transform.GetComponent<IDAgent>();
        if (_idAgent == null)
            _idAgent = _transform.gameObject.AddComponent<IDAgent>();

        _idAgent.globalID = _globalID;
    }

    /// <summary>
    /// 待机
    /// </summary>
    public virtual void Idle()
    {
        // 播放待机动作
        if (_animation != null)
            _animation.CrossFade("idle", 0.3f);
    }

    /// <summary>
    /// 移动
    /// </summary>
    /// <param name="dest"></param>
    public virtual void Move(Vector3 dest)
    {
        // 播放移动动作
        if (_animation != null)
            _animation.CrossFade("walk", 0.3f);
    }

    /// <summary>
    /// 受伤
    /// </summary>
    public void Wound(int hp)
    {
        _attribute.hp -= hp;

        if (_attribute.hp <= 0)
        {
            _attribute.hp = 0;
            Die();
        }
        else
        {
            float percent = (float)_attribute.hp / (float)_initAttibute.hp;

            // 血量改变
            hpChanged(this, hp, BloodTextType.Damage);
        }
    }

    public virtual void Die()
    {
        if (_animation != null)
        {
            _animation.Play("death");
        }
    }

    private void UnSpawn()
    {
        PoolManager.instance.Unspawn(_transform.gameObject);
    }

    public virtual void Update(float dt)
    {
        if (_animation != null &&  !_animation.isPlaying)
        {
            _animation.CrossFade("idle");
        }
    }


    public virtual void Attack(int skillID, uint targetid)
    {
    }

    public virtual Dictionary<int, Character> GetTypeEnemies(RoleType type)
    {
        return null;
    }

    public virtual Character GetNearestEnemyInSight(RoleType type)
    {
        return null;
    }

    public virtual void SyncHeroPos(Vector3 dest, Vector3 farward)
    {
    }
}

