﻿using System;
using System.Collections.Generic;
using UnityEngine;
using common;
using Vector3 = UnityEngine.Vector3;

public class SkillContainer : Character
{
    // 技能容器
    protected Dictionary<int, SkillShooter> _skillShooters = new Dictionary<int, SkillShooter>();

    // 无效的技能索引
    private const int _invalidSkillIndex = -1;

    public SkillContainer(LunaNavmeshQuery navmesh, RoleConfig config, RoleData data) : base(navmesh, config, data)
    {
        // 获取角色拥有的所有技能配置
        Dictionary<int, SkillGroupCfg> skillGroups = ConfigManager.instance.GetSkillGroup(_attribute.roleID);

        // 创建技能发射器并添加到容器中
        foreach (SkillGroupCfg cfg in skillGroups.Values)
        {
            SkillShooter shooter = new SkillShooter();
            SkillSpellCfg spellCfg = ConfigManager.instance.GetSkillSpell(cfg.ID);
            SkillBulletCfg bulletCfg = ConfigManager.instance.GetBullet(cfg.ID);

            shooter.Init(this, spellCfg, bulletCfg);
            if (!_skillShooters.ContainsKey(spellCfg.ID))
                _skillShooters.Add(spellCfg.ID, shooter);
        }
    }

    public override void Update(float dt)
    {
        base.Update(dt);

        // 更新技能发射器
        Dictionary<int, SkillShooter>.Enumerator _shooterEnumerator = _skillShooters.GetEnumerator();
        while (_shooterEnumerator.MoveNext())
        {
            KeyValuePair<int, SkillShooter> skillShooterPair = _shooterEnumerator.Current;
            SkillShooter shooter = skillShooterPair.Value;
            shooter.Update(dt);
        }
    }


    /// <summary>
    /// 获取技能发射器
    /// </summary>
    /// <param name="skillID">技能ID</param>
    /// <returns></returns>
    public SkillShooter GetSkillShooter(int skillID)
    {
        foreach (SkillShooter shooter in _skillShooters.Values)
        {
            if (shooter.skillID == skillID)
                return shooter;
        }

        return null;
    }

    /// <summary>
    /// 获取已经冷却完成的技能发射器
    /// </summary>
    /// <returns></returns>
    public SkillShooter GetCoolCompleteSkillShooter()
    {
        SkillShooter shooter = null;
        foreach (SkillShooter s in _skillShooters.Values)
        {
            if (s.CDComplete())
            {
                shooter = s;
            }
        }

        return shooter;
    }

    /// <summary>
    /// 播放技能动作和特效
    /// </summary>
    /// <param name="skillID"></param>
    private void PlaySkill(int skillID)
    {
        SkillSpellCfg spellCfg = ConfigManager.instance.GetSkillSpell(skillID);

        // 播放动作
        if (_animation != null && spellCfg.AnimName != null)
            _animation.Play(spellCfg.AnimName);

        // 从对象池中拿取特效并播放特效
        if (spellCfg.CastEffect != null)
        {
            GameObject fxGo = PoolManager.instance.Spawn("FX/", spellCfg.CastEffect);
            Battle.instance.AddEffect(fxGo);
            PlayEffect(fxGo, _transform.position, _transform.rotation);
        }
    }

    /// <summary>
    /// 播放特效
    /// </summary>
    /// <param name="go">特效游戏对象</param>
    /// <param name="pos">播放位置</param>
    /// <param name="rotation">播放的朝向</param>
    public static void PlayEffect(GameObject go, Vector3 pos, Quaternion rotation)
    {
        // 初始化特效对象的坐标，朝向和大小
        go.transform.position = pos;
        go.transform.rotation = rotation;
        go.transform.localScale = Vector3.one;

        // 播放特效父对象的粒子效果
        ParticleSystem parentPs = go.GetComponent<ParticleSystem>();
        parentPs.Clear();
        parentPs.time = 0;
        parentPs.Play();

        // 播放特效子对象的粒子效果
        ParticleSystem[] psArray = go.GetComponentsInChildren<ParticleSystem>();
        for (int i = 0; i < psArray.Length; i++)
        {
            ParticleSystem ps = psArray[i];
            ps.Clear();
            ps.time = 0;
            ps.Play();
        }
    }

    /// <summary>
    /// 判断目标是否在攻击范围之内
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    public bool InAttackRange(Character target)
    {
        float distance = Vector3.Distance(position, target.position);
        return distance <= attackRange;
    }

    /// <summary>
    /// 攻击目标
    /// </summary>
    /// <param name="index">技能索引</param>
    public void Attack(SkillShooter shooter)
    {
        shooter.Shoot(target);

        // 攻击时面向目标
        _transform.forward = (target.position - position).normalized;

        // 播放技能动作和特效
        PlaySkill(shooter.skillID);
    }

    public override void Attack(int skillID, uint targetid)
    {
        Character target = Battle.instance.GetRole(targetid);
        if (target == null)
        {
            return;
        }

        if (type != RoleType.Doctor && type != RoleType.Tower)
        {
            _transform.forward = (target.position - position).normalized;
        }


        PlaySkill(skillID);

        SkillShooter shooter = GetSkillShooter(skillID);

        shooter.Shoot(target);
    }
    /// <summary>
    /// 追击
    /// </summary>
    /// <param name="target"></param>
    public void Chase(Character target, int skillIndex = _invalidSkillIndex)
    {
        if (InAttackRange(target))
        {
            if (skillIndex == _invalidSkillIndex)
            {
                SkillShooter shooter = GetCoolCompleteSkillShooter();
                if (shooter != null)
                {
                    BattleSyncSender.RoleAttack((uint)shooter.skillID, (uint)target.globalID);
                }
            }
            else
            {
                // 释放技能
                int skillID = ConfigManager.instance.GetSkillID(_attribute.roleID, skillIndex);

                SkillShooter shooter = GetSkillShooter(skillID);
                if (shooter.CDComplete())
                {
                    BattleSyncSender.RoleAttack((uint)shooter.skillID, (uint)target.globalID);
                }
            }
        }
        else
        {
            BattleSyncSender.RoleMove(target.position);
        }
    }

}

