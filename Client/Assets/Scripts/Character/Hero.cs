﻿
using common;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class Hero : Mover
{
    public Hero(LunaNavmeshQuery navmesh, RoleConfig config, RoleData data) : base(navmesh, config, data)
    {

    }

    public override void SyncHeroPos(Vector3 dest, Vector3 farward)
    {
        _transform.position = dest;
        _transform.forward = farward;
    }
}

