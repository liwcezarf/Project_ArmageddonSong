﻿using System;
using System.Collections.Generic;
using common;


/// <summary>
/// 角色属性
/// </summary>
public class CharacterAttribute
{
    public int roleID;

    // 角色类型
    public RoleType roleType;

    // 阵营
    public RoleSide roleSide;

    public int hp;
    public int mp;
    
    // 攻击范 围
    public float attackRange;

    // 视野
    public float sight;

    public CharacterAttribute Clone()
    {
        return this.MemberwiseClone() as CharacterAttribute;
    }

    /// <summary>
    /// 将配置转换为角色属性
    /// </summary>
    /// <param name="cfg"></param>
    /// <param name="side"></param>
    /// <returns></returns>
    public static CharacterAttribute GetAttribute(RoleConfig cfg, RoleSide side)
    {
        CharacterAttribute attr = new CharacterAttribute();
        attr.roleID = cfg.ID;
        attr.roleType = cfg.RoleType;
        attr.roleSide = side;
        attr.hp = cfg.Hp;
        attr.mp = cfg.Mp;
        attr.attackRange = cfg.AttackRange;
        attr.sight = cfg.Sight;

        return attr;
    }
}