﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 子弹类技能
/// </summary>
public class SkillBullet
{
    // 施法者
    private Character _caster;

    // 目标
    private Character _target;

    // 创建这个子弹的法术
    private SkillSpell _spell;

    // 子弹类型
    private BulletType _type;

    // 子弹命中后结束更新
    private bool _end = false;

    // 子弹飞行速度
    private float _flySpeedH = 0f;

    private bool _track = false;

    private float _splitAngle = 0f;

    // 子弹游戏对象的变换信息
    private Transform _transform;

    private Vector3 _targetPos;

    private const float _hitDistance = 0.8f;


    public bool end
    {
        get
        {
            return _end;
        }
    }

    public SkillBullet(Character caster, Character target, SkillBulletCfg bulletCfg, SkillSpell spell, Vector3 targetPos)
    {
        _caster = caster;
        _target = target;
        _flySpeedH = bulletCfg.FlySpeedH;
        _spell = spell;
        _type = bulletCfg.BulletType;
        _track = bulletCfg.FlyTrack == 1 ? true : false;
        _splitAngle = bulletCfg.SplitAngle;
        _targetPos = targetPos;

        _transform = (PoolManager.instance.Spawn("FX/", bulletCfg.BulletEffect)).transform;
        _transform.position = caster.position + new Vector3(0, 0.8f, 0);
        _transform.localScale = Vector3.one;
        _transform.rotation = Quaternion.identity;
    }

    public void Update(float dt)
    {
        if (_type == BulletType.Cast || _type == BulletType.Multiple)
        {
            Vector3 targetpos = _target.position + new Vector3(0, 0.8f, 0);
            float distance = Vector3.Distance(_transform.position, targetpos);

            if (distance <= _hitDistance)
            {
                _spell.HitTarget(_target);
                _end = true;
                PoolManager.instance.Unspawn(_transform.gameObject);
            }
            else
            {
                // 子弹朝向
                Vector3 dir = (targetpos - _transform.position).normalized;

                // 每帧的增量
                Vector3 frameDelta = dir * _flySpeedH * dt;

                // 设置子弹的当前坐标
                _transform.position += frameDelta;

                // 设置子弹的朝向
                _transform.forward = dir;
            }
        }
        else if (_type == BulletType.Split)
        {
            Vector3 targetpos = _targetPos + new Vector3(0, 0.8f, 0);
            float distance = Vector3.Distance(_transform.position, _targetPos);

            if (distance <= _hitDistance)
            {
                //_spell.HitTarget(_target);
                _end = true;
                PoolManager.instance.Unspawn(_transform.gameObject);
            }
            else
            {
                Vector3 dir = (_targetPos - _transform.position).normalized;
                Vector3 frameDelta = dir * _flySpeedH * dt;
                _transform.position += frameDelta;
                _transform.forward = dir;
            }
        }
    }

}

