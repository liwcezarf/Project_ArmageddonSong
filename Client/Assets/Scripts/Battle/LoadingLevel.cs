﻿//using System;
//using System.Collections.Generic;
//using UnityEngine;

//public class LoadingLevel
//{
//    public static string levelName;
//    public LoadingLevel()
//    {

//    }

//    public void Init()
//    {
//        WindowManager.instance.CloseAll();

//        WindowManager.instance.Open<LoadingWnd>().Initialize();
//        Timer.Invoke(LoadNextLevel, null, 2f);
//    }

//    private void LoadNextLevel()
//    {
//        WindowManager.instance.Close<LoadingWnd>();
//        Application.LoadLevel(levelName);
//    }
//}

using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;


public class LoadingLevel : Singleton<LoadingLevel>
{
    private string _targetScene;

    public void Initialize()
    {
        WindowManager.instance.CloseAll();
        WindowManager.instance.Open<LoadingWnd>().Initialize();

        Timer.Invoke( () =>
        {
            Finalise();
            SceneManager.LoadScene(_targetScene);
        }, null, 2f);
    }

    public void Finalise()
    {
        WindowManager.instance.Close<LoadingWnd>();
    }

    public void LoadScene(string targetScene)
    {
        _targetScene = targetScene;

        SceneManager.LoadScene("Loading");
    }
}