﻿
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;
using Vector3 = UnityEngine.Vector3;
using common;

public class Battle : Singleton<Battle>
{
    // 导航网格信息，管理导航网格的读取和实时更新
    private LunaNavmeshQuery _navmeshQuery;

    // 玩家控制的英雄
    private Character _mainHero;


    // 选中目标时，角色脚下的标记
    private Transform _targetTip;

    // 关卡中的所有角色
    private Dictionary<int, Character> _roles = new Dictionary<int, Character>();

    // 特效对象的容器
    private List<GameObject> _effects = new List<GameObject>();

    // 子弹容器
    private List<SkillBullet> _bullets = new List<SkillBullet>();

    public LunaNavmeshQuery navmesh { get {  return _navmeshQuery; }  }

    private bool _battleStart = false;


    public void Initialize(LevelConfig levelConfig)
    {
        // 读取导航数据，并初始化导航网格信息
        _navmeshQuery = new LunaNavmeshQuery();
        _navmeshQuery.Initialize("Navmesh/" + levelConfig.Map);

        Object targetTipObj = Resources.Load("Widgets/TargetTip");
        _targetTip = (GameObject.Instantiate(targetTipObj) as GameObject).transform;
        _targetTip.position = new Vector3(10000, 10000, 10000);
        _targetTip.localScale = Vector3.one;

        BattleWnd battleWnd = WindowManager.instance.Open<BattleWnd>();
        battleWnd.Initialize();

        _battleStart = true;
    }

    public void BindMainHero(Character role)
    {
        _mainHero = role;

        //// 获取双方相机
        //GameObject blueSideCamera = GameObject.Find("Camera_Blue");
        //GameObject redSideCamera = GameObject.Find("Camera_Red");

        //blueSideCamera.SetActive(_mainHero.side == RoleSide.Blue);
        //redSideCamera.SetActive(_mainHero.side == RoleSide.Red);

        //_camera = _mainHero.side == RoleSide.Blue ? blueSideCamera.GetComponent<Camera>() : redSideCamera.GetComponent<Camera>();


    }

    public void AddRole(int globalID, Character role)
    {
        if (!_roles.ContainsKey(role.globalID))
        {
            _roles.Add(role.globalID, role);
        }
    }

    public void Update(float dt)
    {

        if (!_battleStart) return;

        // 更新导航网格

        _navmeshQuery.Update(dt);

        foreach (Character role in _roles.Values.ToArray())
        {
            if (!role.alive)
            {
                _roles.Remove(role.globalID);
            }
            else
            {
                role.Update(dt);
            }
        }

        // 如果特效的生存周期已经过去，就将特效对象回收到池中
        for (int i = 0; i < _effects.Count; i++)
        {
            GameObject go = _effects[i];
            ParticleSystem ps = go.GetComponent<ParticleSystem>();
            if (!ps.IsAlive())
            {
                _effects.Remove(go);
                PoolManager.instance.Unspawn(go);
            }
        }

        // 更新子弹，如果子弹命中，就从子弹容器中移除
        for (int i = 0; i < _bullets.Count; i++)
        {
            SkillBullet bullet = _bullets[i];
            bullet.Update(dt);

            if (bullet.end)
            {
                _bullets.Remove(bullet);
            }
        }
    }


    /// <summary>
    /// 将特效对象添加到容器中
    /// </summary>
    /// <param name="go"></param>
    public void AddEffect(GameObject go)
    {
        _effects.Add(go);
    }

    public void AddBullet(SkillBullet bullet)
    {
        _bullets.Add(bullet);
    }

    /// <summary>
    ///  获取某个阵营的所有活着的单位
    /// </summary>
    /// <param name="side"></param>
    /// <returns></returns>
    public Dictionary<int, Character> GetAliveRoles(RoleSide side)
    {
        Dictionary<int, Character> aliveRoles = new Dictionary<int, Character>();
        foreach (Character role in _roles.Values)
        {
            if (role.alive && role.side == side)
                aliveRoles.Add(role.globalID, role);
        }

        return aliveRoles;
    }

    /// <summary>
    /// 获取某一阵营的某种类型的单位
    /// </summary>
    /// <param name="side">阵营</param>
    /// <param name="type">角色类型</param>
    /// <returns></returns>
    public Dictionary<int, Character> GetSideTypeRoles(RoleSide side, RoleType type)
    {
        Dictionary<int, Character> roles = new Dictionary<int, Character>();
        foreach (Character role in _roles.Values)
        {
            if (role.alive && role.side == side && role.type == type)
            {
                roles.Add(role.globalID, role);
            }
        }

        return roles;
    }

    public void Clear()
    {
        PoolManager.instance.Clear();

        _roles.Clear();

        _bullets.Clear();
        _effects.Clear();
    }


    public bool AllMonsterDie()
    {
        bool end = true;
        foreach (Character role in _roles.Values)
        {
            if (role.type == RoleType.Monster && role.alive)
            {
                end = false;
            }
        }
        return end;
    }

    /// <summary>
    /// 创建角色
    /// </summary>
    /// <param name="roledata"></param>
    public void CreateRole(RoleData roledata)
    {
        // 通过服务器传来的角色ID获取当前角色的配置
        RoleConfig config = ConfigManager.instance.GetRoleConfig((int)roledata.RoleID);

        // 创建这个角色，并添加到容器中

        Character role = null;
        switch (roledata.RoleType)
        {
            case RoleType.Hero:
                role = new Hero(navmesh, config, roledata);
                break;
            case RoleType.Soldier:
                role = new Soldier(navmesh, config, roledata);
                break;
            case RoleType.Monster:
                role = new Monster(navmesh, config, roledata);
                break;
            case RoleType.Tower:
                role = new Tower(navmesh, config, roledata);
                break;
            case RoleType.Doctor:
                role = new Doctor(navmesh, config, roledata);
                break;
            case RoleType.MainHero:
                role = new MainHero(navmesh, config, roledata);
                break;
            case RoleType.MainTower:
                role = new MainTower(navmesh, config, roledata);
                break;
        }

        AddRole(role.globalID, role);

        // 如果是玩家自己选的角色
        //if (roledata.accountName == DataCache.instance.accountName)
        //{
        //    BindMainHero(role);
        //}

        BattleWnd battleWnd = WindowManager.instance.GetWnd<BattleWnd>();
        battleWnd.OnCharacterCreate(role);
        role.hpChanged = battleWnd.CreateBloodText;
        role.characterDie = battleWnd.OnCharacterDie;
    }

    /// <summary>
    /// 通过全局唯一ID获取角色对象
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public Character GetRole(uint id)
    {
        if (_roles.ContainsKey((int)id))
            return _roles[(int)id];
        return null;
    }
}