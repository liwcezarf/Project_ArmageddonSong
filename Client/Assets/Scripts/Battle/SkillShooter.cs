﻿using System;
using System.Collections.Generic;


/// <summary>
/// 技能发射器
/// </summary>
public class SkillShooter
{
    /// <summary>
    /// 施法者
    /// </summary>
    private Character _caster;

    /// <summary>
    /// 法术配置
    /// </summary>
    private SkillSpellCfg _spellCfg;

    /// <summary>
    /// 子弹配置
    /// </summary>
    private SkillBulletCfg _bulletCfg;

    /// <summary>
    /// 法术容器
    /// </summary>
    private List<SkillSpell> _spells = new List<SkillSpell>();

    /// <summary>
    ///  冷却时间
    /// </summary>
    private float _cd;

    // 冷却是否开始
    private bool _cdStart = false;

    // 冷却已经过去的时间
    private float _elapsedCDTime = 0f;

    public int skillID
    {
        get
        {
            return _spellCfg.ID;
        }
    }

    /// <summary>
    /// 初始化技能发射器
    /// </summary>
    /// <param name="caster"></param>
    /// <param name="spellCfg"></param>
    public void Init(Character caster, SkillSpellCfg spellCfg, SkillBulletCfg bulletCfg)
    {
        _caster = caster;
        _spellCfg = spellCfg;
        _bulletCfg = bulletCfg;
        _cd = spellCfg.CD;
    }

    /// <summary>
    /// 释放技能
    /// </summary>
    /// <param name="target"></param>
    public void Shoot(Character target)
    {
        SkillSpell spell = new SkillSpell(_caster, target, _spellCfg, _bulletCfg);
        _spells.Add(spell);

        _cdStart = true;
        _elapsedCDTime = 0;
    }

    /// <summary>
    /// 更新技能发射器
    /// </summary>
    /// <param name="dt"></param>
    public void Update(float dt)
    {
        // 更新法术，如果法术结束就移除
        for(int i = 0; i < _spells.Count; i++)
        {
            SkillSpell spell = _spells[i];
            spell.Update(dt);

            if (spell.end)
                _spells.Remove(spell);
        }

        // 更新技能冷却计时器
        if (_cdStart)
        {
            if (_elapsedCDTime >= _cd)
            {
                _cdStart = false;
                _elapsedCDTime = 0;
            }

            _elapsedCDTime += dt;
        }
    }

    /// <summary>
    /// 冷却完成
    /// </summary>
    /// <returns></returns>
    public bool CDComplete()
    {
        return !_cdStart;
    }
}