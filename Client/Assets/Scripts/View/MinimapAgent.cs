﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class MinimapAgent
{
    private Transform _transform;

    private Character _ch;

    public MinimapAgent(Transform parent, Transform model, Character ch)
    {
        _ch = ch;

        _transform = GameObject.Instantiate(model.gameObject).transform;
        _transform.SetParent(parent);
        _transform.localPosition = new Vector3(_ch.position.x - 150, _ch.position.z - 100, 0) * 1.26f;
        _transform.localScale = new Vector3(100, 100, 1);
        _transform.gameObject.SetActive(true);
    }

    public void Update(float dt)
    {
        _transform.localPosition = new Vector3(_ch.position.x - 150, _ch.position.z - 100, 0) * 1.26f;
    }
}

