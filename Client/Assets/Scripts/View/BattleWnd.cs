﻿
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public enum BloodTextType
{
    None = 0,
    Damage = 1,
    Recover = 2,

}

/// <summary>
/// 战斗界面
/// </summary>
public class BattleWnd : BaseWindow
{
    // 返回按钮
    private Button _btnReturn;

    // 提示文本
    private Text _tipsText;

    // 头顶的信息栏
    private Dictionary<Character, FloatingBar> _floatingBars = new Dictionary<Character, FloatingBar>();

    private Dictionary<Character, MinimapAgent> _minimapAgents = new Dictionary<Character, MinimapAgent>();

    public void Initialize()
    {
        _btnReturn = _wndTran.FindChild("BtnReturn").GetComponent<Button>();
        _btnReturn.onClick.AddListener(OnReturnBtnClick);

        _tipsText = _wndTran.FindChild("TipsText").GetComponent<Text>();
        _tipsText.text = string.Empty;
    }

    private void OnReturnBtnClick()
    {
        Battle.instance.Clear();
        LoadingLevel.instance.LoadScene("Main");
    }

    /// <summary>
    /// 创建飘血文字
    /// </summary>
    /// <param name="position">世界坐标</param>
    /// <param name="hp">血量</param>
    /// <param name="type">数值类型</param>
    public void CreateBloodText(Character ch, int hp, BloodTextType type)
    {
        // 对象池创建
        Transform transform = PoolManager.instance.Spawn("UI/", "BloodText").transform;
        transform.parent = _wndTran;

        transform.position = WorldToScreenPosition(ch.position + new Vector3(0, 2, 0));
        transform.localScale = Vector3.one;

        Vector3 pos = transform.localPosition;
        pos.z = 0;
        transform.localPosition = pos;


        Text text = transform.GetComponent<Text>();
        text.text = hp.ToString();

        transform.DOLocalMoveY(transform.localPosition.y + 100, 0.5f);

        Timer.Invoke(() =>
        {
            PoolManager.instance.Unspawn(transform.gameObject);
        }, null, 1.0f);

        float percent = (float)ch.hp / (float)ch.totalhp;
        _floatingBars[ch].HpChange(percent);
    }

    /// <summary>
    /// 世界坐标转屏幕坐标
    /// </summary>
    /// <param name="position"></param>
    public static Vector3 WorldToScreenPosition(Vector3 position)
    {
        Vector3 screenPosition = Camera.main.WorldToScreenPoint(position);
        Vector3 uipoint = WindowManager.instance.uiCamera.ScreenToWorldPoint(screenPosition);
        uipoint.z = 0;
        return uipoint;
    }

    /// <summary>
    /// 显示提示
    /// </summary>
    /// <param name="content">显示的内容</param>
    public void ShowTips(string content)
    {
        _tipsText.text = content;
        _tipsText.transform.localPosition = new Vector3(-395f, -119, 0);
        _tipsText.color = Color.red;

        Timer.Invoke(() =>
        {
            _tipsText.transform.DOLocalMoveY(_tipsText.transform.position.y + 100, 0.5f);
            DOTween.ToAlpha(() => _tipsText.color, a => _tipsText.color = a, 0f, 0.5f);
        }, null, 0.5f);
    }

    /// <summary>
    /// 角色出生
    /// </summary>
    /// <param name="ch"></param>
    public void OnCharacterCreate(Character ch)
    {
        FloatingBar bar = new FloatingBar(_wndTran, ch);
        _floatingBars.Add(ch, bar);

        // 小地图代理
        Transform minimap = _wndTran.FindChild("MiniMap");
        Transform model = ch.side == common.RoleSide.Blue ? minimap.FindChild("hero_blue") : minimap.FindChild("hero_red");
        MinimapAgent agent = new MinimapAgent(minimap, model, ch);
        
        _minimapAgents.Add(ch, agent);
    }

    /// <summary>
    /// 角色死亡
    /// </summary>
    /// <param name="ch"></param>
    public void OnCharacterDie(Character ch)
    {
        _floatingBars[ch].Unspawn();
        _floatingBars.Remove(ch);
    }

    public override void Update(float dt)
    {
        foreach (KeyValuePair<Character, FloatingBar> pair in _floatingBars)
        {
            Character ch = pair.Key;
            FloatingBar bar = pair.Value;

            bar.UpdatePosition(ch.position + new Vector3(0, 2, 0));
        }

        foreach (KeyValuePair<Character, MinimapAgent> pair in _minimapAgents)
        {
            Character ch = pair.Key;
            MinimapAgent agent = pair.Value;
            agent.Update(dt);
        }
    }

    /// <summary>
    /// 关闭界面
    /// </summary>
    public override void Close()
    {
        base.Close();

        _floatingBars.Clear();
    }
}

