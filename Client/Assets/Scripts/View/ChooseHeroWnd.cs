﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using client;
using server;
using common;
using Vector3 = UnityEngine.Vector3;

/// <summary>
/// 选择英雄界面
/// </summary>
public class ChooseHeroWnd : BaseWindow
{
    private int _myAccountIndex = -1;

    private Button _btnOK;

    // 我的匹配信息
    private MatchAccountInfo _myMatchAccount;

    public int myHeroID;


    public class ButtonClickListener : MonoBehaviour, IPointerClickHandler
    {
        // 英雄配置ID
        public uint herocfgid;

        public ChooseHeroWnd chooseWnd;

        public void OnPointerClick(PointerEventData eventData)
        {
            chooseWnd.myHeroID = (int)herocfgid;

            MatchAccountInfo mai = chooseWnd._myMatchAccount;
            chooseWnd.OnUpdateSelect(mai.RoleSide, mai.Index, herocfgid);
        }
    }

    public void Init(NotifyChooseHero chooseHero)
    {
        Transform  _optionHeros = _wndTran.FindChild("OptionalHeros/Viewport/Content");
        Transform _optHero = _wndTran.FindChild("OptionalHeros/Viewport/Button");

        _btnOK = _wndTran.FindChild("BtnOk").GetComponent<Button>();
        _btnOK.onClick.AddListener(OnBtnOk);

        // 备选英雄
        for (int i = 0; i < chooseHero.OptionalHeros.Count;i++)
        {
            Transform optHeroTran = (GameObject.Instantiate(_optHero.gameObject)).transform;
            optHeroTran.parent = _optionHeros;

            optHeroTran.gameObject.SetActive(true);
            uint heroid = chooseHero.OptionalHeros[i];
            optHeroTran.name = heroid.ToString();
            optHeroTran.localScale = Vector3.one;
            optHeroTran.localPosition = Vector3.zero;
            

            string headPic = ConfigManager.instance.GetRoleConfig((int)heroid).HeadPic;
            optHeroTran.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/halfphoto/" + headPic);

            ButtonClickListener bcl = optHeroTran.gameObject.AddComponent<ButtonClickListener>();
            bcl.herocfgid = heroid;
            bcl.chooseWnd = this;
        }


        // 获取我的匹配信息
        for (int i = 0; i < chooseHero.Accounts.Count; i++)
        {
            if (chooseHero.Accounts[i].AccountName == DataCache.instance.accountName)
            {
                _myMatchAccount = chooseHero.Accounts[i];
            }
        }
    }

    private void OnBtnOk()
    {
        ClientMsg req = new ClientMsg();
        req.Ready = new client.ReqReady();
        req.Ready.HeroID = (uint)myHeroID;

        NetworkManager.instance.Send((int)MsgID.ReqReady, req);
    }

    public void OnAccountReady(NotifyReady ready)
    {
        _btnOK.enabled = false;
        _btnOK.transform.FindChild("Text").GetComponent<Text>().text = "已选择";
        OnUpdateSelect(ready.side, ready.index, ready.heroid);
    }

    /// <summary>
    /// 更新选择的英雄
    /// </summary>
    /// <param name="side">阵营</param>
    /// <param name="index">索引</param>
    /// <param name="heroid">英雄配置id</param>
    public void OnUpdateSelect(RoleSide side, uint index, uint heroid)
    {
        Transform parent = side == RoleSide.Blue ? _wndTran.FindChild("BlueSide") : _wndTran.FindChild("RedSide");
        Image halfphoto = parent.FindChild("Scroll View/Viewport/Content/" + index.ToString()).GetComponent<Image>();
        string headPic = ConfigManager.instance.GetRoleConfig((int)heroid).HeadPic;
        halfphoto.sprite = Resources.Load<Sprite>("Icon/halfphoto/" + headPic);
    }
}