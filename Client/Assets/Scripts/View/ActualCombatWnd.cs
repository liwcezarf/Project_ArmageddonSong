﻿using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;
using client;

/// <summary>
/// 实战窗口
/// </summary>
public class ActualCombatWnd : BaseWindow
{

    public class ButtonClickListener : MonoBehaviour, IPointerClickHandler
    {
        public uint limitPlayerNumber;
        public void OnPointerClick(PointerEventData eventData)
        {
            ClientMsg req = new ClientMsg();
            req.ActualCombat = new client.ReqActualCombat();
            req.ActualCombat.PlayerNumber = limitPlayerNumber;

            NetworkManager.instance.Send((int)MsgID.ReqActualCombat, req);
        }
    }


    public void Init()
    {
        Transform content = _wndTran.FindChild("Scroll View/Viewport/Content");
        Transform btnBattle = _wndTran.FindChild("Scroll View/Viewport/Button");

        Dictionary<int, LevelConfig> battleCfgs = ConfigManager.instance.levelConfigs;
        foreach (LevelConfig cfg in battleCfgs.Values)
        {
            Transform child = GameObject.Instantiate(btnBattle.gameObject).transform;
            child.SetParent(content);
            child.localScale = Vector3.one;
            child.localPosition = Vector3.zero;
            child.gameObject.SetActive(true);

            child.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/choosebattle/" + cfg.ID);

            child.gameObject.AddComponent<ButtonClickListener>().limitPlayerNumber = (uint)cfg.PlayerNumber;
        }
    }
}