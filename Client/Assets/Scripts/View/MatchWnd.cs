﻿using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
///  匹配窗口
/// </summary>
public class MatchWnd : BaseWindow
{
    private Text _text;
    private Button _btnCancel;

    private common.LevelType _levelType;

    private int _timeInSecond = 0;
    public void Init(common.LevelType levelType)
    {
        _levelType = levelType;
        _text = _wndTran.FindChild("Text").GetComponent<Text>();
        _btnCancel = _wndTran.FindChild("BtnCancel").GetComponent<Button>();

        UIEventListener.Get(_btnCancel.gameObject).onPointerClick = OnCancel;

        Timer.InvokeRepeat(ShowText, Cancel, 1000);
    }

    /// <summary>
    /// 往服务端发送取消匹配请求
    /// </summary>
    /// <param name="ped"></param>
    private void OnCancel(PointerEventData ped)
    {
        client.ClientMsg req = new client.ClientMsg();
        req.CancelMatch = new client.ReqCancelMatch();
        req.CancelMatch.LevelType = _levelType;
        NetworkManager.instance.Send((int)MsgID.ReqCancelMatch, req);
        Cancel();
    }

    private void Cancel()
    {

    }

    private void ShowText()
    {
        _text.text = string.Format("Match Time:{0}", _timeInSecond++); 
    }
}