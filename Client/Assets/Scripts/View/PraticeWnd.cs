﻿using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using client;

public class PraticeWnd : BaseWindow
{
    private Button _btn1V1;
    private Button _btn3V3;
    private Button _btn5V5;
    private Button _btnClose;

    private uint _playerNumber;

    public void Init()
    {
        _btn1V1 = _wndTran.FindChild("Btn_1V1").GetComponent<Button>();
        _btn3V3 = _wndTran.FindChild("Btn_3V3").GetComponent<Button>();
        _btn5V5 = _wndTran.FindChild("Btn_5V5").GetComponent<Button>();
        _btnClose = _wndTran.FindChild("Close").GetComponent<Button>();

        UIEventListener.Get(_btn1V1.gameObject).onPointerClick = On1V1;
        UIEventListener.Get(_btn3V3.gameObject).onPointerClick = On3V3;
        UIEventListener.Get(_btn5V5.gameObject).onPointerClick = On5V5;
        UIEventListener.Get(_btnClose.gameObject).onPointerClick = OnClose;
    }

    private void On1V1(PointerEventData ped)
    {
        _playerNumber = 1;
        ChooseLevel();
    }

    private void On3V3(PointerEventData ped)
    {
        _playerNumber = 3;
        ChooseLevel();
    }

    private void On5V5(PointerEventData ped)
    {
        _playerNumber = 5;
        ChooseLevel();
    }

    private void ChooseLevel()
    {
        ClientMsg req = new ClientMsg();
        req.PraticeLevel = new client.ReqPraticeLevel();
        req.PraticeLevel.PlayerNumber = _playerNumber;

        //NetworkManager.instance.Send((int)MsgID.ReqPraticeLevel, req);
        NetworkManager.instance.Send((int)MsgID.ReqPraticeLevel, req);
    }

    private void OnClose(PointerEventData ped)
    {
        WindowManager.instance.Close<PraticeWnd>();
    }
}