﻿
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;


/// <summary>
/// 角色头顶信息栏
/// </summary>
public class FloatingBar
{
    // 血条
    private Slider _hpBar;

    // 角色名
    private Text _roleName;

    /// <summary>
    /// 构造
    /// </summary>
    /// <param name="globalid">角色全局ID</param>
    public FloatingBar(Transform parent, Character ch)
    {
        _hpBar = PoolManager.instance.Spawn("UI/", "FloatingBar").GetComponent<Slider>();
        _hpBar.transform.parent = parent;
        _hpBar.transform.localScale = Vector3.one;

        _roleName = _hpBar.transform.FindChild("RoleName").GetComponent<Text>();
        //_roleName.text = string.Format("{0}--{1}", ch.GlobalID, ch.name);
    }

    /// <summary>
    /// 血量改变
    /// </summary>
    /// <param name="percent">血量百分比</param>
    public void HpChange(float percent)
    {
        _hpBar.value = percent;
    }

    /// <summary>
    /// 当角色对象被回收时，信息栏也要回收
    /// </summary>
    public void Unspawn()
    {
        PoolManager.instance.Unspawn(_hpBar.gameObject);
    }

    public void UpdatePosition(Vector3 position)
    {
        _hpBar.transform.position = BattleWnd.WorldToScreenPosition(position);

        Vector3 pos = _hpBar.transform.localPosition;
        pos.z = 0;
        _hpBar.transform.localPosition = pos;
    }
}
