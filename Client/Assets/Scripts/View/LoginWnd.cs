﻿
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using client;

public class LoginWnd : BaseWindow
{
    private Text _textAccount;

    private Text _textPassword;

    private InputField _editAccount;

    private InputField _editPassword;

    private Button _btnRegister;

    private Button _btnLogin;

    private string _keyAccount = "Account";
    private string _keyPassword = "Password";

    public void Init()
    {
        _textAccount = _wndTran.FindChild("TextAccount").GetComponent<Text>();
        _textPassword = _wndTran.FindChild("TextPassword").GetComponent<Text>();
        _editAccount = _wndTran.FindChild("EditAccount").GetComponent<InputField>();
        _editPassword = _wndTran.FindChild("EditPassword").GetComponent<InputField>();
        _btnRegister = _wndTran.FindChild("BtnRegister").GetComponent<Button>();
        _btnLogin = _wndTran.FindChild("BtnLogin").GetComponent<Button>();

        UIEventListener.Get(_btnRegister.gameObject).onPointerClick = OnRegister;
        UIEventListener.Get(_btnLogin.gameObject).onPointerClick = OnLogin;


        string prefAccount = PlayerPrefs.GetString(_keyAccount);
        if (string.IsNullOrEmpty(prefAccount))
        {
            PlayerPrefs.SetString(_keyAccount, _editAccount.text);
        }
        else
        {
            _editAccount.text = PlayerPrefs.GetString(_keyAccount);
        }


        string prefPassword = PlayerPrefs.GetString(_keyPassword);
        if (string.IsNullOrEmpty(prefPassword))
        {
            PlayerPrefs.SetString(_keyPassword, _editPassword.text);
        }
        else
        {
            _editPassword.text = PlayerPrefs.GetString(_keyPassword);
        }
    }

    private void CheckStringLength()
    {
        if (_textAccount.text.Length > 20)
        {
            MessageBox.Show("Account Name too long");
        }

        if (_textPassword.text.Length > 20)
        {
            MessageBox.Show("Password too long");
        }
    }

    private void OnRegister(PointerEventData ped)
    {
        // 检测字符串长度
        CheckStringLength();

        // 创建注册消息实例
        ClientMsg req = new ClientMsg();
        req.Register = new ReqRegister();
        req.Register.Account = _editAccount.text;
        req.Register.Password = _editPassword.text;

        // 将注册消息发送到服务端

        NetworkManager.instance.Send((int)MsgID.ReqRegist, req);

        // 将账号和密码保存到注册表
        PlayerPrefs.SetString(_keyAccount, _editAccount.text);
        PlayerPrefs.SetString(_keyPassword, _editPassword.text);
    }

    private void OnLogin(PointerEventData ped)
    {
        CheckStringLength();

        ClientMsg req = new ClientMsg();
        req.Login = new ReqLogin();
        req.Login.AccountName = _editAccount.text;
        req.Login.Password = _editPassword.text;
        NetworkManager.instance.Send((int)MsgID.ReqLogin, req);

        PlayerPrefs.SetString(_keyAccount, _editAccount.text);
        PlayerPrefs.SetString(_keyPassword, _editPassword.text);
    }
}