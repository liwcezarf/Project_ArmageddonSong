﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingWnd : BaseWindow
{
    // 已经过去的时间
    private float _ealpsedTime = 0f;

    private const float _totalTime = 2f;

    private Slider _slider;
    public void Initialize()
    {
        _slider = _wndTran.FindChild("Slider").GetComponent<Slider>();

        _ealpsedTime = 0;
    }

    public override void Update(float dt)
    {
        _slider.value = _ealpsedTime / _totalTime;

        _ealpsedTime += dt;
    }
}