﻿//using System;
//using System.Collections.Generic;
//using UnityEngine.UI;
//using UnityEngine.EventSystems;

///// <summary>
///// 选择战斗类型，实战/练习
///// </summary>
//public class MainWnd : BaseWindow
//{
//    private Button _btnActualCombat;

//    private Button _btnPratice;

//    public void Init()
//    {
//        _btnActualCombat = _wndTran.FindChild("BtnActualCombat").GetComponent<Button>();
//        _btnPratice = _wndTran.FindChild("BtnPratice").GetComponent<Button>();

//        UIEventListener.Get(_btnActualCombat.gameObject).onPointerClick = OnActualCombat;
//        UIEventListener.Get(_btnPratice.gameObject).onPointerClick = OnPratice;
//    }

//    private void OnActualCombat(PointerEventData ped)
//    {
//        WindowManager.instance.Open<ActualCombatWnd>().Init();
//    }

//    private void OnPratice(PointerEventData ped)
//    {
//        WindowManager.instance.Open<PraticeWnd>().Init();
//    }
//}