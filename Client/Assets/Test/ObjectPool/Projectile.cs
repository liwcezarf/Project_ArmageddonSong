﻿//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;

//public enum ProjectileTrackType{
//	STRAIGHT = 1,//直线
//	THROW 	 = 2,
//	SPECIAL  = 3,//特殊函数
//	FREE	 = 4,//自由弹道，策划控制
//}

//public enum ProjectileType{
//	NORMAL = 1,
//	FOLLOW = 2,
//}
//[behaviac.TypeMetaInfo()]
//public class Projectile : MapObject {

//	//TODO 设置销毁函数
//	private static ObjectPool<Projectile> _pool = new ObjectPool<Projectile>(1, 30);
//	public static void ClearPool(){
//		_pool.Free();
//	}

//	private enum State{
//		PREPARE,
//		RUNNING,
//		END,
//		BACKUP,
//	}

//	[behaviac.MemberMetaInfo("selfProjectile", "子弹自己")]
//	public Projectile selfProjectile{
//		get{
//			return this;
//		}
//	}

//	[behaviac.MemberMetaInfo("owner", "拥有子弹的角色")]
//	public Character owner;

//	[behaviac.MemberMetaInfo("refSkill", "发射子弹的技能")]
//	public IWSkill refSkill;

//	[behaviac.MemberMetaInfo("isFacingRight", "发射子弹方向")]
//	public bool isFacingRight{
//		get{
//			return motionAgent.velocity.x > 0;
//		}
//	}

//	[behaviac.MemberMetaInfo("skillMainTarget", "技能当前主目标")]
//	public Character skillMainTarget{
//		get{
//			return refSkill.mainTarget;
//		} 
//		set{
//			refSkill.mainTarget = value;
//		}
//	}

//	[behaviac.MemberMetaInfo("skillNowLoopTarget", "技能当前主目标", true)]
//	public Character skillNowLoopTarget{
//		get{
//			return refSkill.nowLoopTarget;
//		}
//	}

//	[behaviac.MemberMetaInfo("tempTarget", "临时目标")]
//	public Character tempTarget;

//	[behaviac.MemberMetaInfo("skillLoopIndex", "技能当前目标数量", true)]
//	public int skillLoopIndex{
//		get{
//			return refSkill.loopIndex;
//		}
//	}

//	[behaviac.MemberMetaInfo("configData", "子弹的配置信息")]
//	public config.skills.bulletInfo configData {
//		get{
//			if(_configData == null)
//				_configData = Xml2Object.Get<config.skills.bulletInfo>(typeID);

//			return _configData;
//		}
//	}

//	[behaviac.MemberMetaInfo("isOnEnter", "表示是否子弹刚出生", true)]
//	public bool isOnEnter{ 
//		get{
//			var o = _onEnter;
//			//			LogHelper.LogDebug("isOnEnter == " + o);
//			_onEnter = false;
//			return o;
//		} 
//	}

//	[behaviac.MemberMetaInfo("isOnHit", "是否在这帧有hit触发", true)]
//	public bool isOnHit{
//		get{
//			var o = _onHit;
//			//			LogHelper.LogDebug("isOnHit == " + o);
//			_onHit = false;
//			return o;
//		}
//	}

//	[behaviac.MemberMetaInfo("isOnDestroy", "子弹销毁", true)]
//	public bool isOnDestroy{
//		get{
//			var o = _onDestroy;
//			//			LogHelper.LogDebug("isOnHit == " + o);
//			_onDestroy = false;
//			return o;
//		}
//	}




//	public Character mainProjectileTarget;

//	private int delayTime;
//	private State _state;
//	private config.skills.bulletInfo _configData;
//	private List<int> oldTargetIDList;
//	private bool _onHit 		= false;
//	private bool _onDestroy 	= false;
//	private bool _onEnter 		= false;



//	public Projectile( ):base(0, Camp.ANT){
//		_state = State.PREPARE;
//		oldTargetIDList = new List<int>();

//		CreatePAgents();
//	}
		
//	//CreateAgents 默认在iwinit调用，对象池方式会被反复调用
//	public override void CreateAgents ()
//	{
//	}

//	public void CreatePAgents () {

//		motionAgent = new MotionAgentProjectile();
//		spriteAgent = new SpriteAgentBase();

//		spriteAgent.animObj.SetActive(false);

//	}

//	public void InitProjectile ( int projectileTypeID, Character owner, IWSkill skill,  Character target, IWVector2 pos, IWVector2 velocity, int maxTime, int delayTime = 0 ) {
//		_configData = null;
//		oldTargetIDList.Clear();
//		typeID = projectileTypeID;
//		this.delayTime = delayTime;
//		this.owner = owner;
//		this.refSkill = skill;
//		this.mainProjectileTarget = target;
//		_onHit = false;
//		_onDestroy = false;
//		_onEnter = true;
//		objTemp1 = objTemp2 = objTemp3 = objTemp4 = objTemp5 = objTemp6 = 0;
//		tempTarget = null;
//		_state = State.PREPARE;

//		UnityEngine.Profiler.BeginSample("Projectile Load tree");
//		Debug.Log("load bullet " + configData.bulletScript);
//		btload(configData.bulletScript);
//		btsetcurrent(configData.bulletScript);
//		UnityEngine.Profiler.EndSample();

//		UnityEngine.Profiler.BeginSample("Projectile Init MotionAgentProjectile");
//		(motionAgent as MotionAgentProjectile ).InitProjectile( this, pos, velocity, maxTime );
////		spriteAgent.animObj.SetActive(true);
//		spriteAgent.position = (Vector3)location;
//		UnityEngine.Profiler.EndSample();
//	}

//	public static int AddProjectile ( int projectileTypeID, Character owner, IWSkill skill, Character target, IWVector2 pos, IWVector2 velocity, int maxTime, int delayTime = 0 ) {
////		Debug.Log("AddProjectile");
//		UnityEngine.Profiler.BeginSample("Projectile Get");
//		var prj = _pool.Get();
//		BattleAgent.instance.addObject(prj);
//		UnityEngine.Profiler.EndSample();

//		UnityEngine.Profiler.BeginSample("Projectile InitProjectile");
//		prj.InitProjectile( projectileTypeID, owner, skill, target, pos, velocity, maxTime, delayTime);
//		UnityEngine.Profiler.EndSample();
////		for (int i = 1; i <= 33; i++) {
////			for( int j = (100 - i) / 2; j >= i; j-- ){
////				//三个值 i, j, 100 - i -j
////			}
////		}
////
//		return prj.id;
//	}




//	//子弹弹射
////	[behaviac.MethodMetaInfo( "BoundToDirection", "朝某个角色为方向弹射，不保证命中，有弹道型子弹可用" )]
////	public void BoundToDirection ( Character target ){
////		
////
////	}

//	[behaviac.MethodMetaInfo( "BoundToTarget", "朝某个角色弹射,跟踪子弹可用" )]
//	public void BoundToTarget (Character target) {
////		Debug.Log("BoundToTarget");

//		_configData = null;
//		oldTargetIDList.Clear();
//		(motionAgent as MotionAgentProjectile ).BoundToTarget( target );

//	}

//	[behaviac.MethodMetaInfo( "IsTempTargetNull", "临时目标是否是空" )]
//	public bool IsTempTargetNull () {
//		return tempTarget == null;

//	}

//	[behaviac.MethodMetaInfo( "SetPositionRelativeToOwner", "设置相对于子弹释放者的位置，默认朝右" )]
//	public void SetPositionRelativeToOwner ( int x, int y, bool lockRight = false ) {
//		if(owner.isFacingRight == false && lockRight == false )
//			x = -x;

//		fixedLocation = owner.fixedLocation + new IWVector2( IWMath.CreateDistance(x), IWMath.CreateDistance(y) );
//	}

//	[behaviac.MethodMetaInfo( "SetPositionRelativeToOwnerCenter", "设置相对于子弹释放者的中心位置位置，默认朝右" )]
//	public void SetPositionRelativeToOwnerCenter ( int x, int y, bool lockRight = false ) {
//		if(owner.isFacingRight == false && lockRight == false )
//			x = -x;

//		fixedLocation = owner.fixedCenter + new IWVector2( IWMath.CreateDistance(x), IWMath.CreateDistance(y) );
//	}

//	[behaviac.MethodMetaInfo( "Translate", "移动子弹x, y的距离" )]
//	public void Translate( int x, int y ){

//		fixedLocation += new IWVector2( IWMath.CreateDistance(x), IWMath.CreateDistance(y) );
//	}

//	public override void IWUpdate (){
//		if( _state == State.PREPARE){
//			delayTime -= GameClock.fixedDeltaMillTime;
//			if(delayTime <= 0){
//				spriteAgent.animObj.SetActive(true);
//				_state = State.RUNNING;
//			}
//		}

//		if(_state == State.RUNNING){

//			//跟随型子弹判断目标是否还在
//			if(configData.bulletType == (int)ProjectileType.FOLLOW ){
//				if(mainProjectileTarget == null || mainProjectileTarget.isDead == true ){
//					OnProjectileDestroy();
//					return;
//				}
//			}
//			btexec();

//			motionAgent.IWUpdate();
//			spriteAgent.position = (Vector3)motionAgent.location;
////			SyncPosition();
//		}


//		if(_state == State.END){
//			(motionAgent as MotionAgentProjectile ).OnDestroy();
//			spriteAgent.animObj.SetActive(false);
//			_state = State.BACKUP;
//			_pool.Put(this);
////			OnExit();
//		}
//	}

//	public void OnHit ( List<Character> targets ) {
		
//		if(configData.collideMemory == 0){
//			var count = oldTargetIDList.Count;

//			for (int j = targets.Count - 1; j >= 0; j--) {
//				var t = targets[j];

//				bool has = false;

//				for (int i = 0; i < count; i++) {
//					if(t.id == oldTargetIDList[i]){
//						targets.RemoveAt(j);
//						has = true;
//						break;
//					}
//				}

//				if(has == false)
//					oldTargetIDList.Add(t.id);
//			}


//		}

//		//目标全部都被过滤就不做
//		if(targets.Count <= 0)
//			return;

//		_onHit = true;
//		refSkill.targets = targets;

//		btexec();
//	}

//	[behaviac.MethodMetaInfo( "OnProjectileDestroy", "子弹销毁" )]
//	public void OnProjectileDestroy(){
//		if(_state != State.RUNNING)
//			return;
		
////		Debug.Log("OnProjectileDestroy");
//		_onDestroy = true;
//		btexec();
//		_state = State.END;
//		BattleAgent.instance.RemoveObject(this);
//	}

//	public void OnRotate(IWVector2 speed){
//		//根据速度计算角度
//		var a =  Vector2.Angle( speed.ToVector2(), Vector2.right );
//		spriteAgent.RotateTo( Vector2.Angle( speed.ToVector2(), Vector2.right ) );
//	}
//}
