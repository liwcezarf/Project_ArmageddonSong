﻿using System;
using System.Collections;
using System.Collections.Generic;


/// <summary>
/// 对象池   , 一旦 对象池被 Free 了， 则不能再进行使用
/// </summary>
/// <typeparam name="T"> 对象的类型 </typeparam>
public class ObjectPool<T> where T : new()
{
    /// <summary>
    /// 最大的对象数量  
    /// </summary>
    public int MaxObjectNum { get; set; }
    /// <summary>
    /// 当前对象的数量
    /// </summary>
    public int Count { get; private set; }
    /// <summary>
    /// 空闲的对象数量
    /// </summary>
    public int FreeCount { get { return _freestack.Count ; } }
    /// <summary>
    /// 当前对象池是不是被Free 了
    /// </summary>
    public bool IsDispose { get { return _isDispose; } }

    private bool _isDispose = false;
    private Func<T> _createFunc;
    private Action<T> _disposeFunc;
    private Action<T> _resetFunc;
    private Stack<T> _freestack = new Stack<T>();

    /// <summary>
    /// 创建方法会 默认的使用一个new()
    /// </summary>
    public ObjectPool()
    {
        _createFunc = () => { return new T(); };
        _disposeFunc = null;
        MaxObjectNum = 200;
        Count = 0;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="createFunc"></param>
    /// <param name="disposeFunc"></param>
    public ObjectPool(Func<T> createFunc, Action<T> disposeFunc)
    {
        this._createFunc = createFunc;
        this._disposeFunc = disposeFunc;
        MaxObjectNum = 200;
        Count = 0;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="initNum">初始数量</param>
    /// <param name="maxNum">最大数量</param>
    public ObjectPool(int initNum , int maxNum ) : this()
    {
        this.MaxObjectNum = maxNum;
        for (int i = 0; i < initNum; i++)
        {
            CreateAndPutInPool();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="initNum">初始数量</param>
    /// <param name="createFunc">创建对象的方法</param>
    /// <param name="disposeFunc">销毁对象的方法</param>
    public ObjectPool(int initNum, Func<T> createFunc, Action<T> disposeFunc) : this(createFunc, disposeFunc)
    {
        for (int i = 0; i < initNum; i++)
        {
            CreateAndPutInPool();

        }
    }

    /// <summary>
    /// <para>从对象池中 Get 一个可用的对象</para>
    /// 如果当前没有可用的对象 ，则创建一个对象并返回
    /// 如果当前的对象数量 大于等于 MaxObjectNum 属性， 则返回  default(T)
    /// ！！使用结束，请调用 Put(T) 方法将对象放回对象池！
    /// </summary>
    /// <returns></returns>
    public T Get()
    {
        T a = _GetFree();
        if (a == null)
        {
            if (this.Count < MaxObjectNum)
            {
                CreateAndPutInPool();
                return _GetFree();
            }

			return _createFunc();
        }

        return a;
    }

    /// <summary>
    /// 设置   重置状态的方法
    /// </summary>
    /// <param name="func"></param>
    /// <returns></returns>
    public ObjectPool<T> SetResetFunc(Action<T> func)
    {
        this._resetFunc = func;
        return this;
    }

    /// <summary>
    /// 将对象 放回对象池， 使其变的可用
    /// 如果当前对象池已经被 FREE 掉了， 则会对该对象 调用  disposeFunc 方法对其进行销毁操作 并返回
    /// </summary>
    /// <param name="t"></param>
    public void Put(T t)
    {
        if (IsDispose)
        {
            if (_disposeFunc != null)
            {
                _disposeFunc(t);
            }
            return;
        }

        if (typeof(T).IsClass || typeof(T).IsInterface)
        {
            if (t == null)
            {
                return;
            }
        }

        if (_resetFunc != null)
        {
            _resetFunc(t);
        }

//        lock (_freestack)
        {
            _freestack.Push(t);
        }
    }

    /// <summary>
    /// 从对象池中删除单个对象
    /// </summary>
    /// <param name="t"></param>
    public void Free(T t)
    {
        if (_disposeFunc != null)
        {
            _disposeFunc(t);
        }

    }

    /// <summary>
    /// 销毁整个对象池 ,已经在使用的对象 不接受
    /// </summary>
    public void Free()
    {
//        lock (this)
        {
//            _isDispose = true;

            if (_disposeFunc != null)
            {
                foreach (var item in _freestack)
                {
                    _disposeFunc(item);
                }
            }

            _freestack.Clear();
        }
    }

    /// <summary>
    /// 经过Create 的对象不会放入对象池， 请使用 Get() 方法获取一个新的对象
    /// </summary>
    /// <returns></returns>
    public T Create()
    {
        return _createFunc();
    }

    /// <summary>
    /// 创建一个对象， 丢进对象池 , 本方法并不检测 MaxObjectNum
    /// </summary>
    public void CreateAndPutInPool()
    {
        if (IsDispose)
        {
            return;
        }

        T t = _createFunc();
        if (typeof(T).IsClass || typeof(T).IsInterface)
        {
            if (t == null)
            {
                return ;
            }
        }

//        lock (_freestack)
        {
            Count++;
            _freestack.Push(t);
        }
    }

    /// <summary>
    /// 从 free 栈中获取一个对象， 会从free 栈中移除该对象
    /// </summary>
    /// <returns></returns>
    private T _GetFree()
    {
//        lock (_freestack)
        {
            if (_freestack.Count <= 0)
            {
                return default(T);
            }

            return _freestack.Pop();
        }

    }

}


