﻿using System;

/// <summary>
/// 对象池工厂
/// </summary>
class ObjectPoolFactory
{
    /// <summary>
    /// 创建对象池
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="createFunc"></param>
    /// <param name="disposeFunc"></param>
    /// <returns></returns>
    public static ObjectPool<T> CreatePool<T>(Func<T> createFunc, Action<T> disposeFunc) where T : new()
    {
        return new ObjectPool<T>(createFunc, disposeFunc);
    }

    /// <summary>
    /// 创建对象池
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="createFunc"></param>
    /// <returns></returns>
    public static ObjectPool<T> CreatePool<T>(Func<T> createFunc) where T : new()
    {
        return new ObjectPool<T>(createFunc, null);
    }

    /// <summary>
    /// 创建对象池
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static ObjectPool<T> CreatePool<T>() where T : new()
    {
        return new ObjectPool<T>();
    }

    /// <summary>
    /// 创建对象池
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="initNum"></param>
    /// <param name="createFunc"></param>
    /// <param name="disposeFunc"></param>
    /// <returns></returns>
    public static ObjectPool<T> CreatePool<T>(int initNum, Func<T> createFunc, Action<T> disposeFunc) where T : new()
    {
        return new ObjectPool<T>(initNum, createFunc, disposeFunc);
    }

    /// <summary>
    /// 创建对象池
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="initNum"></param>
    /// <param name="maxNum"></param>
    /// <param name="createFunc"></param>
    /// <param name="disposeFunc"></param>
    /// <returns></returns>
    public static ObjectPool<T> CreatePool<T>(int initNum, int maxNum, Func<T> createFunc, Action<T> disposeFunc) where T : new()
    {
        ObjectPool<T> pool = new ObjectPool<T>(initNum, createFunc, disposeFunc);
        pool.MaxObjectNum = maxNum;
        return pool;
    }

    /// <summary>
    /// 创建对象池
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="initNum"></param>
    /// <param name="maxNum"></param>
    /// <returns></returns>
    public static ObjectPool<T> CreatePool<T>(int initNum, int maxNum ) where T : new()
    {
        ObjectPool<T> pool = new ObjectPool<T>(initNum, maxNum);
        return pool;
    }
}

