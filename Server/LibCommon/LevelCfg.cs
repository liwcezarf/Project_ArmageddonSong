﻿using common;
using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// 关卡配置
/// </summary>
public class LevelConfig
{
    public int ID;
    public string Name;
    public LevelType Type;
    public int PlayerNumber;
    public string Scene;
    public string Map;
}

/// <summary>
/// 出生点配置
/// </summary>
public class BornPointCfg
{
    public int ID;
    public RoleType RoleType;

    public RoleSide Side;
    public int LevelID;
    public int RoleID;
    public string InitPosition;
    public string InitRotation;
}

partial class ConfigManager
{
    /// <summary>
    /// 关卡配置
    /// </summary>
    private Dictionary<int, LevelConfig> _levelConfigs = new Dictionary<int, LevelConfig>();

    /// <summary>
    /// 出生点配置
    /// </summary>
    private Dictionary<int, BornPointCfg> _bornPointConfigs = new Dictionary<int, BornPointCfg>();


    public Dictionary<int, LevelConfig> levelConfigs
    {
        get
        {
            return _levelConfigs;
        }
    }
    /// <summary>
    /// 通过关卡ID获取关卡配置
    /// </summary>
    /// <param name="levelID"></param>
    /// <returns></returns>
    public LevelConfig GetLevelCfg(int levelID)
    {
        if (_levelConfigs.ContainsKey(levelID))
        {
            return _levelConfigs[levelID];
        }
        return null;
    }


    public LevelConfig GetLevel(common.LevelType type, int playerNumber)
    {
        foreach (LevelConfig cfg in _levelConfigs.Values)
        {
            if (cfg.Type == type && cfg.PlayerNumber == playerNumber)
            {
                return cfg;
            }
        }

        return null;
    }
    public LevelConfig GetLevel(int ID)
    {
        if (_levelConfigs.ContainsKey(ID))
        {
            return _levelConfigs[ID];
        }
        else
        {
            //MobaLog.Log("Can not find LevelConfig:" + ID);
            return null;
        }
    }

    public BornPointCfg GetBornPoint(int levelID, common.RoleType type, RoleSide side)
    {
        foreach (BornPointCfg cfg in _bornPointConfigs.Values)
        {
            if (cfg.LevelID == levelID && cfg.RoleType == type && cfg.Side == side)
            {
                return cfg;
            }
        }
        return null;
    }

    public Dictionary<int, BornPointCfg> GetBornPointCfgs(int levelID)
    {
        Dictionary<int, BornPointCfg> bornPointConfigs = new Dictionary<int, BornPointCfg>();
        foreach (BornPointCfg cfg in _bornPointConfigs.Values)
        {
            if (cfg.LevelID == levelID && !bornPointConfigs.ContainsKey(levelID))
            {
                bornPointConfigs.Add(levelID, cfg);
            }
        }

        return bornPointConfigs;
    }

}