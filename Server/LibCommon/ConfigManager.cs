﻿
using System;
using common;

/// <summary>
/// 游戏配置
/// </summary>
public partial class ConfigManager : Singleton<ConfigManager>
{
    private IConfigParser _configParser;

    public void Initialize(IConfigParser configParser)
    {
        _configParser = configParser;
        LoadConfigs();
    }

    /// <summary>
    /// 加载配置
    /// </summary>
    public void LoadConfigs()
    {
        _levelConfigs = _configParser.LoadConfig<LevelConfig>("Map");
        _bornPointConfigs = _configParser.LoadConfig<BornPointCfg>("BornPoint");

        _roleConfigs = _configParser.LoadConfig<RoleConfig>("Character");

        _skillGroupConfigs = _configParser.LoadConfig<SkillGroupCfg>("SkillGroup");
        _skillSpellConfigs = _configParser.LoadConfig<SkillSpellCfg>("SkillSpell");
        _skillBulletConfigs = _configParser.LoadConfig<SkillBulletCfg>("SkillBullet");
    }


    /// <summary>
    /// 将字符串转换为向量
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static Vector3 Str2Vec3(string str)
    {
        string[] strArray = str.Split(',');

        Vector3 v = new Vector3();
        v.x = Convert.ToSingle(strArray[0]);
        v.y = Convert.ToSingle(strArray[1]);
        v.z = Convert.ToSingle(strArray[2]);

        return v;
    }
}
