﻿using System;
using System.Collections.Generic;
using System.Linq;
using common;
using server;


/// <summary>
/// 一场战斗
/// </summary>
public class Battle
{
    // 全局唯一角色id计数器
    private uint _globalRoleIdCounter = 1001;

    private bool _startSoldierBorn = false;

    private float _elapsedTime = 0f;

    private float _soldierBornInterval = 20f;

    public uint globalID;

    public LevelType leveltype;

    public LevelConfig config;

    public LunaNavmeshQuery navmeshQuery;

    public bool running = false;

    public Dictionary<string, Account> accounts = new Dictionary<string, Account>();

    // 子弹
    private List<SkillBullet> _bullets = new List<SkillBullet>();


    // 所有单位
    private Dictionary<uint, Character> _roles = new Dictionary<uint, Character>();

    // 只有英雄数据
    private Dictionary<uint, RoleData> _heroDatas = new Dictionary<uint, RoleData>();

    // 英雄重生数据
    private Dictionary<uint, RebornData> _rebornDatas = new Dictionary<uint, RebornData>();


    public void Init(LevelType type, int playerNumber)
    {
        running = true;

        leveltype = type;
        config = ConfigManager.instance.GetLevel(type, playerNumber);

        navmeshQuery = new LunaNavmeshQuery();
        string filePath = "Navmesh/" + config.Map + ".xml";
        navmeshQuery.Initialize(filePath);

    }
    private void TickRoles(float dt)
    {
        if (_startSoldierBorn)
        {
            TickSoldierBorn(dt);
        }

        TickHeroReborn(dt);

        TickRolesUpdate(dt);
    }

    private void TickHeroReborn(float dt)
    {
        foreach (RebornData data in _rebornDatas.Values.ToArray())
        {
            if (data.end)
            {
                HeroReborn(data);
                _rebornDatas.Remove(data.globalid);
            }
            else
            {
                data.Tick(dt);
            }
        }
    }

    private void TickSoldierBorn(float dt)
    {
        if (_elapsedTime >= _soldierBornInterval)
        {
            _elapsedTime = _elapsedTime - _soldierBornInterval;

            SoldierBorn();
        }

        _elapsedTime += dt;
    }

    private void TickRolesUpdate(float dt)
    {
        foreach (Character role in _roles.Values.ToArray())
        {
            if (role.alive)
            {
                role.Update(dt);
            }
            else
            {
                _roles.Remove(role.globalID);
            }
        }
    }

    public void TowerBorn()
    {
        Dictionary<int, BornPointCfg> lbps = ConfigManager.instance.GetBornPointCfgs(config.ID);

        foreach (BornPointCfg lbp in lbps.Values)
        {
            if (lbp.RoleType == RoleType.Tower || lbp.RoleType == RoleType.Doctor)
            {
                common.RoleData roleData = GetRoleData(lbp.RoleID, lbp);

                roleData.UniID = _globalRoleIdCounter;
                Character role = new Tower(this, roleData);


                AddRole(role);
            }
        }
    }


    public void SoldierBorn()
    {
        Dictionary<int, BornPointCfg> lbps = ConfigManager.instance.GetBornPointCfgs(config.ID);

        ServerMsg notify = new ServerMsg();
        notify.RoleBorn = new server.NotifyRoleBorn();
        foreach (BornPointCfg lbp in lbps.Values)
        {
            if (lbp.RoleType != RoleType.Soldier) continue;

            common.RoleData roleData = GetRoleData(lbp.RoleID, lbp);
            roleData.UniID = _globalRoleIdCounter;
            Character role = new Solider(this, roleData);

            AddRole(role);

            notify.RoleBorn.RoleList.Add(roleData);
        }

        MsgSender.BroadCast(accounts, MsgID.RespRoleBorn, notify);

        if (!_startSoldierBorn)
        {
            _startSoldierBorn = true;
        }
    }

    public RoleData HeroBorn(int roleid, RoleSide side, string pos, string rot, string accountName)
    {
        RoleData roleData = GetRoleData(roleid, side, pos, rot, accountName);
        roleData.UniID = _globalRoleIdCounter;

        Character role = new Hero(this, roleData);
        AddHeroData(roleData);
        AddRole(role);

        return roleData;
    }

    private void HeroReborn(RebornData data)
    {
        RoleData roleData = GetHeroData(data.globalid);
        Character role = new Hero(this, roleData);

        AddRole(role);

        ServerMsg notify = new ServerMsg();
        notify.RoleBorn = new NotifyRoleBorn();
        notify.RoleBorn.RoleList.Add(roleData);
        MsgSender.BroadCast(accounts, MsgID.RespRoleBorn, notify);
    }


    private RoleData GetHeroData(uint globalRoleID)
    {
        RoleData roleData = null;
        if (_heroDatas.ContainsKey(globalRoleID))
            roleData = _heroDatas[globalRoleID];


        return roleData;
    }

    private RoleData GetRoleData(int roleid, BornPointCfg lbp)
    {
        RoleConfig cfg = ConfigManager.instance.GetRoleConfig(roleid);
        RoleData roleData = new RoleData();
        roleData.RoleID = (uint)cfg.ID;
        roleData.RoleSide = lbp.Side;
        roleData.RoleType = cfg.RoleType;
        roleData.SightRange = cfg.Sight;
        roleData.Hp = cfg.Hp;
        roleData.Mp = cfg.Mp;
        roleData.Pos = ProtoHelper.Str2ProtoVec(lbp.InitPosition);
        roleData.AttackRange = cfg.AttackRange;

        return roleData;
    }

    private RoleData GetRoleData(int roleid, RoleSide side, string pos, string rot, string accountName)
    {
        RoleConfig cfg = ConfigManager.instance.GetRoleConfig(roleid);
        RoleData roleData = new RoleData();
        roleData.RoleID = (uint)cfg.ID;
        roleData.RoleSide = side;
        roleData.RoleType = cfg.RoleType;
        roleData.SightRange = cfg.Sight;
        roleData.Hp = cfg.Hp;
        roleData.Mp = cfg.Mp;
        roleData.Pos = ProtoHelper.Str2ProtoVec(pos);
        roleData.Dir = ProtoHelper.Str2ProtoVec(rot);
        roleData.AttackRange = cfg.AttackRange;
        roleData.accountName = accountName;

        return roleData;
    }

    private void AddRole(Character role)
    {
        if (!_roles.ContainsKey(role.globalID))
        {
            _roles.Add(role.globalID, role);

            if (role.globalID >= _globalRoleIdCounter)
                _globalRoleIdCounter++;
        }
    }

    public Character GetRole(uint globalRoleID)
    {
        if (_roles.ContainsKey(globalRoleID))
        {
            return _roles[globalRoleID];
        }

        return null;
    }

    private void AddHeroData(RoleData roleData)
    {
        if (!_heroDatas.ContainsKey(roleData.UniID))
            _heroDatas.Add(roleData.UniID, roleData);
    }

    public void AddRebornData(RebornData data)
    {
        if (!_rebornDatas.ContainsKey(data.globalid))
            _rebornDatas.Add(data.globalid, data);
    }

    public List<RoleData> GetRoleDataByType(RoleType type)
    {
        List<RoleData> roleDatas = new List<RoleData>();

        foreach (Character role in _roles.Values)
        {
            if (role.type == type)
            {
                roleDatas.Add(role.originalData);
            }
        }
        return roleDatas;
    }

    public List<RoleData> GetAllRoleDatas()
    {
        List<RoleData> roleDatas = new List<RoleData>();

        foreach (Character role in _roles.Values)
        {
            roleDatas.Add(role.originalData);
        }
        return roleDatas;
    }

    /// <summary>
    /// 玩家加入
    /// </summary>
    /// <param name="acc"></param>
    public void AddAccount(Account acc)
    {
        if (!accounts.ContainsKey(acc.accountName))
        {
            accounts.Add(acc.accountName, acc);
        }
        acc.globalLevelID = globalID;

        Console.WriteLine("Account: {0} join Level: {1}", acc.accountName, globalID);
    }

    public void RemoveAccount(Account acc)
    {
        if (accounts.ContainsKey(acc.accountName))
        {
            accounts.Remove(acc.accountName);
        }
    }

    /// <summary>
    /// 战斗更新
    /// </summary>
    /// <param name="dt"></param>
    public void Update(float dt)
    {
        navmeshQuery.Update(dt);

        TickRoles(dt);

        TickBullets(dt);
    }

    public void TickBullets(float dt)
    {
        for (int i = 0; i < _bullets.Count; i++)
        {
            SkillBullet bullet = _bullets[i];
            if (bullet.end)
            {
                _bullets.Remove(bullet);
            }
            else
            {
                bullet.Update(dt);
            }
        }
    }

    public void AddBullet(SkillBullet bullet)
    {
        _bullets.Add(bullet);
    }
    /// <summary>
    /// 获取某一阵营的活着的单位
    /// </summary>
    /// <param name="side"></param>
    /// <returns></returns>
    public Dictionary<uint, Character> GetAliveRoles(RoleSide side)
    {
        Dictionary<uint, Character> roles = new Dictionary<uint, Character>();

        foreach (Character role in _roles.Values)
        {
            if (role.side == side && role.hp > 0)
                roles.Add(role.globalID, role);
        }

        return roles;
    }

    /// <summary>
    /// 获取某一阵营的某种类型的单位
    /// </summary>
    /// <param name="side"></param>
    /// <param name="type"></param>
    /// <returns></returns>
    public Dictionary<uint, Character> GetAliveRoles(RoleSide side, RoleType type)
    {
        Dictionary<uint, Character> roles = new Dictionary<uint, Character>();

        foreach (Character role in _roles.Values)
        {
            if (role.side == side && role.type == type && role.alive)
                roles.Add(role.globalID, role);
        }

        return roles;

    }

    /// <summary>
    /// 获取敌方单位
    /// </summary>
    /// <param name="side"></param>
    /// <returns></returns>
    public Dictionary<uint, Character> GetEnemies(RoleSide side)
    {
        if (side == RoleSide.Blue)
        {
            return GetAliveRoles(RoleSide.Red);
        }
        else if (side == RoleSide.Red)
        {
            return GetAliveRoles(RoleSide.Blue);
        }
        else if (side == RoleSide.Neutrality)
        {
            return null;
        }

        return null;
    }

    /// <summary>
    /// 获取敌方某种类型的单位
    /// </summary>
    /// <param name="side"></param>
    /// <returns></returns>
    public Dictionary<uint, Character> GetEnemies(RoleSide side, RoleType type)
    {
        if (side == RoleSide.Blue)
        {
            return GetAliveRoles(RoleSide.Red, type);
        }
        else if (side == RoleSide.Red)
        {
            return GetAliveRoles(RoleSide.Blue, type);
        }
        else if (side == RoleSide.Neutrality)
        {
            return null;
        }

        return null;
    }

    /// <summary>
    /// 获取非中立单位
    /// </summary>
    /// <returns></returns>
    public Dictionary<uint, Character> GetNonNeutral()
    {
        Dictionary<uint, Character> roles = new Dictionary<uint, Character>();

        foreach (Character role in _roles.Values)
        {
            if (role.side != RoleSide.Neutrality)
                roles.Add(role.globalID, role);
        }

        return roles;
    }
}

