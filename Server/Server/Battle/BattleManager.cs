﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;

public class BattleManager : Singleton<BattleManager>
{
    private uint LevelID = 1001;

    private Dictionary<uint, Battle> _levelDic;

    public BattleManager()
    {
        _levelDic = new Dictionary<uint, Battle>();
    }

    public void Tick(float dt)
    {
        foreach (Battle level in _levelDic.Values.ToArray())
        {
            if (level.running)
            {
                level.Update(dt);
            }
            else
            {
                _levelDic.Remove(level.globalID);

            }
        }
    }

    /// <summary>
    /// 创建新的战斗
    /// </summary>
    /// <param name="LevelID"></param>
    /// <param name="userRole"></param>
    public Battle Create(common.LevelType type, int playerNumber)
    {
        Battle level = new Battle();
        level.Init(type, playerNumber);
        AddLevel(level);

        Console.WriteLine("Create Level:{0}", level.globalID);
        return level;
    }

    /// <summary>
    /// 添加一场战斗
    /// </summary>
    /// <param name="level"></param>
    public void AddLevel(Battle level)
    {
        level.globalID = LevelID;
        _levelDic.Add(LevelID, level);
        LevelID++;

        LogManager.Log("Add Level: " + level.globalID);
    }

    /// <summary>
    /// 移除一场战斗
    /// </summary>
    /// <param name="globalID"></param>
    public void RemoveLevel(uint UniID)
    {

        if (_levelDic.ContainsKey(UniID))
        {
            _levelDic.Remove(UniID);
        }
        LogManager.Log("Remove Level: " + UniID);
    }

    public Battle GetLevel(uint UniID)
    {
        if (_levelDic.ContainsKey(UniID))
        {
            return _levelDic[UniID];
        }
        return null;
    }
}
