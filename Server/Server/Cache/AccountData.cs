﻿using System;
using System.Collections.Generic;
using common;

public class AccountData
{
    public int id;
    public string account;
    public string pwd;

    public AccountData()
    {

    }

    public AccountData(int id, string account, string pwd)
    {
        this.id = id;
        this.account = account;
        this.pwd = pwd;
    }
}


/// <summary>
/// 账号
/// </summary>
public class Account
{
    // 唯一ID
    public uint uid;

    public string accountName;

    // 所在匹配
    public uint globalMatchID;

    // 匹配的索引
    public int matchIndex = -1;

    // 阵营
    public RoleSide roleSide = RoleSide.Blue;

    // 是否准备完成
    public bool ready = false;

    // 所在关卡的唯一ID
    public uint globalLevelID;

    // 关卡中的角色唯一ID
    public uint globalRoleID;

    // 选择的英雄ID
    public uint heroID;

    public UserToken client;
}

public partial class CacheManager :Singleton<CacheManager>
{
    // 已经登录的玩家信息容器
    private Dictionary<uint, Account> _accounts = new Dictionary<uint, Account>();

    public bool IsAccountOnline(uint accountid)
    {
        return _accounts.ContainsKey(accountid);
    }

    /// <summary>
    /// 从容器中移除一个账户
    /// </summary>
    /// <param name="uid"></param>
    public  void RemoveAccount(uint uid)
    {
        if (_accounts.ContainsKey(uid))
        {
            _accounts.Remove(uid);
            LogManager.Log("{0} logout..." + uid);
        }
    }

    public  Account GetAccount(uint uid)
    {
        if (_accounts.ContainsKey(uid))
            return _accounts[uid];

        return null;
    }

    /// <summary>
    /// 客户端登录时设置账号名和密码
    /// </summary>
    /// <param name="uid"></param>
    /// <param name="name"></param>
    /// <param name="pwd"></param>
    public void AccountOnline(uint uid, string accountname, UserToken token)
    {
        Account acc = new Account();
        acc.uid = uid;
        acc.accountName = accountname;
        acc.client = token;

        _accounts.Add(uid, acc);
    }
}