﻿
using System.Collections.Generic;
using common;

/// <summary>
/// 匹配
/// </summary>
public class MatchData
{
    // 匹配ID， 全局唯一
    public uint globalID;

    // 所在阵营的索引
    private int _blueSideIndex = 0;

    private int _redSideIndex = 0;

    // 当前房间的人数
    private int _accountCounter = 1;

    // 战斗的类型
    public LevelType levelType;

    // 房间最大人数限制
    public uint accountNumLimit = 0;

    // 房间中所有账号的信息
    public Dictionary<string, Account> accounts = new Dictionary<string, Account>();


    // 判断当前房间是否已满
    public bool full {  get {  return accounts.Count >= accountNumLimit * 2;}}

    // 该房间所有的人是否已经准备好了
    public bool ready
    {
        get
        {
            foreach (Account acc in accounts.Values)
            {
                if (acc.ready == false)
                {
                    return false;
                }
            }
            return true;
        }
    }


    public MatchData(LevelType type, uint accountNumLimit)
    {
        this.levelType = type;
        this.accountNumLimit = accountNumLimit;
    }

    // 玩家加入房间
    public void AddAccount(Account account)
    {
        if (!accounts.ContainsKey(account.accountName))
        {
            account.roleSide = _accountCounter % 2 == 0 ? RoleSide.Red : RoleSide.Blue;
            _accountCounter++;

            account.globalMatchID = globalID;

            if (account.roleSide == RoleSide.Blue)
            {
                account.matchIndex = _blueSideIndex;
                _blueSideIndex++;
            }
            else if (account.roleSide == RoleSide.Red)
            {
                account.matchIndex = _redSideIndex;
                _redSideIndex++;
            }
            accounts.Add(account.accountName, account);
        }
    }

    // 玩家推出房间
    public void RemoveAccount(Account account)
    {
        if (accounts.ContainsKey(account.accountName))
        {
            accounts.Remove(account.accountName);
            account.globalMatchID = 0;
            account.matchIndex = -1;
            account.ready = false;
        }

        if (accounts.Count <= 0)
        {
            CacheManager.instance.RemoveMatch(this);
        }
    }
}

public partial class CacheManager 
{
    private uint _matchIDCounter = 0;
    private Dictionary<uint, MatchData> _matchs = new Dictionary<uint, MatchData>();


    // 创建一个匹配
    public MatchData CreateMatch(LevelType type, uint accNumLimit)
    {
        MatchData match = new MatchData(type, accNumLimit);
        match.globalID = _matchIDCounter;
        _matchIDCounter++;
        AddMatch(match);

        return match;
    }

    public void AddMatch(MatchData match)
    {
        if (!_matchs.ContainsKey(match.globalID))
        {
            _matchs.Add(match.globalID, match);
        }
    }

    public void RemoveMatch(MatchData match)
    {
        if (_matchs.ContainsKey(match.globalID))
        {
            _matchs.Remove(match.globalID);
        }
    }

    public MatchData GetMatch(uint globalID)
    {
        MatchData match = null;
        if (_matchs.TryGetValue(globalID, out match))
        {
            return match;
        }
        return null;
    }

    // 获取没有满的房间
    public MatchData GetNotFullMatch(common.LevelType levelType, uint accNumLimit)
    {
        MatchData mat = null;
        foreach (MatchData match in _matchs.Values)
        {
            if (match.levelType == levelType && match.accountNumLimit == accNumLimit && !match.full)
            {
                mat = match;
            }
        }
        return mat;
    }
}