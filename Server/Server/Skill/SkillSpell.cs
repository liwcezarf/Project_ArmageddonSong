﻿

using common;
using Vector3 = Luna3D.Vector3;
using System.Collections.Generic;


/// <summary>
/// 法术
/// </summary>
public class SkillSpell
{
    /// <summary>
    /// 施法者
    /// </summary>
    private Character _caster;

    /// <summary>
    /// 法术目标
    /// </summary>
    private Character _target;

    /// <summary>
    /// 法术配置
    /// </summary>
    private SkillSpellCfg _spellCfg;

    /// <summary>
    /// 子弹配置
    /// </summary>
    private SkillBulletCfg _bulletCfg;

    /// <summary>
    /// 已经更新的时间
    /// </summary>
    private float _elapsedTime;

    // 已经攻击的次数
    private int _hitNum = 0;

    // 总攻击次数
    private int _totalHitNum;

    /// <summary>
    /// 法术是否结束
    /// </summary>
    private bool _end;

    public bool end
    {
        get
        {
            return _end;
        }
    }

    public SkillSpell(Character caster, Character target, SkillSpellCfg spellCfg, SkillBulletCfg bulletCfg)
    {
        _caster = caster;
        _target = target;
        _spellCfg = spellCfg;
        _bulletCfg = bulletCfg;
        _totalHitNum = _spellCfg.HitNum;
    }

    public void Update(float dt)
    {
        // 已经过去的时间 >= 命中时间 + 法术攻击次数 * 法术连击间隔
        float deltaTime = _hitNum * _spellCfg.HitInterval;

        if (_elapsedTime >= _spellCfg.HitTime + deltaTime)
        {
            // 如果有子弹配置则创建子弹
            if (_bulletCfg != null)
            {
                CreateBullet();
            }
            else
            {
                // 单体技能直接命中目标
                if (_spellCfg.AreaCenter == AreaType.None)
                {
                    HitTarget(_target);
                }
                // AOE技能判断范围内的敌人，然后命中
                else
                {
                    HitAOE();
                }
            }

            _hitNum++;
            if (_hitNum >= _totalHitNum)
            {
                _end = true;
            }
        }
        _elapsedTime += dt;
    }

    /// <summary>
    /// 命中单体目标
    /// </summary>
    public void HitTarget(Character target)
    {
        int deltaHp = (int)(_spellCfg.BasicNum * 0.2f);
        //int hp = UnityEngine.Random.Range(_spellCfg.BasicNum - deltaHp, _spellCfg.BasicNum + deltaHp);
        target.Damage(_spellCfg.BasicNum);
    }

    /// <summary>
    /// AOE效果
    /// </summary>
    private void HitAOE()
    {
        // 确定AOE技能的作用阵营
        RoleSide side = RoleSide.Neutrality;
        if (_spellCfg.AffectSide == AffectSide.Enemy)
        {
            if (_caster.side == RoleSide.Blue)
                side = RoleSide.Red;
            else if (_caster.side == RoleSide.Red)
                side = RoleSide.Blue;
        }
        else if (_spellCfg.AffectSide == AffectSide.Friend)
        {
            side = _caster.side;
        }

        Dictionary<uint, Character> aliveRoles = _caster.GetAliveRoles(side);
        foreach (Character role in aliveRoles.Values)
        {
            HitTargetInAOE(role);
        }
    }

    /// <summary>
    /// 命中AOE技能范围内的角色
    /// </summary>
    /// <param name="role"></param>
    private void HitTargetInAOE(Character role)
    {
        // 如果AOE技能的范围中心是己方
        if (_spellCfg.AreaCenter == AreaType.Self)
        {
            // 如果AOE技能的形状是圆形
            if (_spellCfg.AreaShape == AreaShape.Circle)
            {
                if (AreaDetection.PointInCircle(_spellCfg.AreaArg1, _caster.position, role.position))
                {
                    HitTarget(role);
                }
            }
            // 如果AOE技能的形状是扇形
            else if (_spellCfg.AreaShape == AreaShape.Fan)
            {
                if (AreaDetection.PointInFan(_spellCfg.AreaArg1, _caster.position, _caster.forward, _spellCfg.AreaArg2, role.position))
                {
                    HitTarget(role);
                }
            }
        }
        else if (_spellCfg.AreaCenter == AreaType.Target)
        {
            // 如果AOE技能的形状是圆形
            if (_spellCfg.AreaShape == AreaShape.Circle)
            {
                if (AreaDetection.PointInCircle(_spellCfg.AreaArg1, _target.position, role.position))
                {
                    HitTarget(role);
                }
            }
        }
    }

    /// <summary>
    /// 创建子弹
    /// </summary>
    private void CreateBullet()
    {
        SkillBullet bullet = new SkillBullet(_caster, _target, _bulletCfg, this, Vector3.zero);
        _caster.AddBullet(bullet);
    }
}