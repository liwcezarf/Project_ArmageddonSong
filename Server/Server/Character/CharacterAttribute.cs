﻿using System;
using System.Collections.Generic;
using common;

/// <summary>
/// 角色属性
/// </summary>
public class CharacterAttribute
{
    /// <summary>
    /// 角色ID
    /// </summary>
    public uint RoleID { get; set; }
    /// <summary>
    /// 角色类型
    /// </summary>
    public RoleType RoleType { get; set; }
    /// <summary>
    /// 英雄类型
    /// </summary>
    public RoleSide RoleSide { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int Hp { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int Mp { get; set; }

    /// <summary>
    /// 视野范围
    /// </summary>
    public float SightRange { get; set; }

    /// <summary>
    /// 攻击范围
    /// </summary>
    public float AttackRange { get; set; }

    public object Clone()
    {
        return this.MemberwiseClone();
    }

    public static CharacterAttribute GetAttribute(common.RoleData data)
    {
        CharacterAttribute attribute = new CharacterAttribute();
        attribute.RoleID = data.RoleID;
        attribute.RoleType = data.RoleType;
        attribute.RoleSide = data.RoleSide;

        attribute.Hp = data.Hp;
        attribute.Mp = data.Mp;
        attribute.SightRange = data.SightRange;
        attribute.AttackRange = data.AttackRange;

        return attribute;
    }
}
