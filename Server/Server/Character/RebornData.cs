﻿
public class RebornData
{
    public RebornData(uint globalid, float rebornTime)
    {
        _globalid = globalid;
        _rebornTime = rebornTime;
    }

    private uint _globalid;

    private float _rebornTime = 0f;

    private float _elpasedTime = 0f;

    private bool _end = false;

    public uint globalid
    {
        get
        {
            return _globalid;
        }
    }

    public bool end
    {
        get
        {
            return _end;
        }
    }

    public void Tick(float dt)
    {
        if(_elpasedTime >= _rebornTime)
        {
            _end = true;
        }
        else
        {
            _elpasedTime += dt;
        }
    }
}
