﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using common;
using Luna3D;

/// <summary>
/// 小兵
/// </summary>
public class Solider : Mover
{
    public Solider(Battle level, RoleData data) : base(level, data)
    {
    }

    public override void Update(float dt)
    {
        base.Update(dt);

        if (!alive) return;

        _target = FindNearestTypeEnemyInSight(RoleType.Soldier);

        if (_target == null)
            _target = FindNearestTypeEnemyInSight(RoleType.Hero);

        if (_target == null)
            _target = FindNearestTypeEnemy(RoleType.Tower);


        if (_target != null)
            Chase(_target);
    }

    public override void Move(Luna3D.Vector3 dest)
    {
        base.Move(dest);

        BattleMsgSender.NotifyMove(globalID, dest, _battle.accounts);
    }
}