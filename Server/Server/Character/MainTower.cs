﻿
using common;

/// <summary>
/// 主基地
/// </summary>
public class MainTower : Tower
{
    public MainTower(Battle level, RoleData data) : base(level, data)
    {
    }

    public override void Die()
    {
        base.Die();

        _battle.running = false;
    }
}

