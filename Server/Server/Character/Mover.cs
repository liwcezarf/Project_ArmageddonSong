﻿using common;

/// <summary>
/// 可移动单位
/// </summary>
public class Mover : Behavior
{

    public Mover(Battle level, RoleData data) : base(level, data)
    {
        // 导航代理
        _navAgent = new LunaNavAgent();
        _navAgent.Init(this, _transform, level.navmeshQuery);
    }

    public override void Move(Luna3D.Vector3 dest)
    {
        if (!_navAgent.enabled)
            _navAgent.enabled = true;

        _navAgent.Move(dest);
    }

    public override void Idle()
    {
        if (_navAgent.enabled)
        {
            _navAgent.enabled = false;
            BattleMsgSender.NotifyIdle(globalID, _battle.accounts);
        }
    }

    public override void Update(float dt)
    {
        base.Update(dt);
        
        if(alive)
            _navAgent.Update(dt);
    }
}
