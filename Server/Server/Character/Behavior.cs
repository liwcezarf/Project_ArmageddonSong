﻿
using System.Collections.Generic;
using System.Linq;

using Vector3 = Luna3D.Vector3;
using Mathf = Luna3D.Mathf;
using common;

/// <summary>
/// 角色行为
/// </summary>
public class Behavior : SkillContainer
{
    private const int _recuperateHp = 100;

    public Behavior(Battle level, RoleData data) : base(level, data)
    {
    }

    /// <summary>
    /// 从列表中查找视野范围内的最近单位
    /// </summary>
    /// <param name="roles"></param>
    /// <returns></returns>
    public Character FindNearestRoleInSight(Dictionary<uint, Character> roles)
    {
        Character role = null;

        float minDistance = Mathf.Infinity;
        foreach (Character enemy in roles.Values)
        {
            float distance = Vector3.Distance(enemy.position, position);
            if (distance < _attribute.SightRange && distance < minDistance && enemy.alive)
            {
                role = enemy;
                minDistance = distance;
            }
        }

        return role;
    }

    /// <summary>
    /// 查找距离自己最近的某种类型的敌人，可能不在视野范围内
    /// </summary>
    /// <param name="self"></param>
    /// <returns></returns>
    public override Character FindNearestTypeEnemy(RoleType type)
    {
        Dictionary<uint, Character> enemies = _battle.GetEnemies(_initAttribute.RoleSide, type);

        Character role = null;
        float minDistance = Mathf.Infinity;
        foreach (Character enemy in enemies.Values)
        {
            float distance = Vector3.Distance(enemy.position, position);
            if (distance < minDistance)
            {
                role = enemy;
                minDistance = distance;
            }
        }

        return role;
    }

    /// <summary>
    /// 查找视野范围内的距离自己最近的敌方单位
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public override Character FindNearestTypeEnemyInSight(RoleType type)
    {
        Dictionary<uint, Character> enemies = _battle.GetEnemies(_initAttribute.RoleSide, type);

        Character role = null;
        float minDistance = Mathf.Infinity;
        foreach (Character enemy in enemies.Values)
        {
            float distance = Vector3.Distance(enemy.position, position);
            if (distance < _attribute.SightRange && distance < minDistance && enemy.alive)
            {
                role = enemy;
                minDistance = distance;
            }
        }

        return role;
    }

    /// <summary>
    /// 查找视野内的可移动敌方单位
    /// </summary>
    /// <returns></returns>
    public Character FindMover()
    {
        Dictionary<uint, Character> enemies = _battle.GetEnemies((RoleSide)_attribute.RoleSide);
        return FindNearestRoleInSight(enemies);
    }

    /// <summary>
    /// 治疗友方单位
    /// </summary>
    public override void TreatFriends()
    {
        Dictionary<uint, Character> friends = _battle.GetAliveRoles(side, RoleType.Hero);
        foreach (Character role in friends.Values.ToArray())
        {
            float distance = Vector3.Distance(role.position, position);
            if (distance < sight)
            {
                role.AddHp(_recuperateHp);
            }
        }
    }
}