﻿
using common;

/// <summary>
/// 防御塔
/// </summary>
public class Tower : SkillContainer
{
    public Tower(Battle level, RoleData data) : base(level, data)
    {

    }

    public override void Update(float dt)
    {
        base.Update(dt);

        if (!alive) return;

        _target = FindNearestTypeEnemyInSight(RoleType.Soldier);

        if (_target == null)
            _target = FindNearestTypeEnemyInSight(RoleType.Hero);

        if (_target == null)
            _target = FindNearestTypeEnemyInSight(RoleType.Monster);

        if (_target != null)
            TryAttack();
    }
}

