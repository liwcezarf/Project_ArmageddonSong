﻿using System;
using System.Collections.Generic;
using common;

/// <summary>
/// 技能容器
/// </summary>
public class SkillContainer : Character
{
    private Dictionary<int, SkillShooter> _skillShooters;

    private float _gcdElapsedTime = 0f;

    // 开始计算GCD
    private bool _gcdStart = false;


    private const float _gcd = 1;

    public SkillContainer(Battle level, RoleData data) : base(level, data)
    {
        _skillShooters = new Dictionary<int, SkillShooter>();

        // 获取角色拥有的所有技能配置
        Dictionary<int, SkillGroupCfg> skillGroups = ConfigManager.instance.GetSkillGroup((int)_attribute.RoleID);

        // 创建技能发射器并添加到容器中
        foreach (SkillGroupCfg cfg in skillGroups.Values)
        {
            SkillShooter shooter = new SkillShooter();
            SkillSpellCfg spellCfg = ConfigManager.instance.GetSkillSpell(cfg.ID);
            SkillBulletCfg bulletCfg = ConfigManager.instance.GetBullet(cfg.ID);

            shooter.Init(this, spellCfg, bulletCfg);
            if (!_skillShooters.ContainsKey(spellCfg.ID))
                _skillShooters.Add(spellCfg.ID, shooter);
        }
    }


    public override void Update(float dt)
    {
        base.Update(dt);

        foreach (SkillShooter shooter in _skillShooters.Values)
        {
            shooter.Update(dt);
        }

        if (_gcdStart)
        {
            if (_gcdElapsedTime >= _gcd)
            {
                _gcdElapsedTime = 0;
                _gcdStart = false;
            }
            else
            {
                _gcdElapsedTime += dt;
            }
        }
    }


    public override bool CanShoot(out SkillShooter shooter)
    {
        shooter = null;

        if (_gcdStart)  return false;

        shooter = GetSkillShooter();
        return shooter == null ? false : true;
    }

    public override void Shoot(SkillShooter shooter, Character target)
    {
        shooter.Shoot(target);

        _gcdStart = true;
        _gcdElapsedTime = 0;
    }

    private SkillShooter GetSkillShooter()
    {
        SkillShooter shooter = null;
        List<SkillShooter> shooters = new List<SkillShooter>();
        foreach (SkillShooter ss in _skillShooters.Values)
        {
            if (ss.CDComplete() && !shooters.Contains(ss))
                shooters.Add(ss);
        }

        foreach (SkillShooter ss in _skillShooters.Values)
        {
            if (ss.CDComplete())
                shooter = ss;
        }
        return shooter;
    }

    public override SkillShooter GetSkillShooter(int skillid)
    {
        if (_skillShooters.ContainsKey(skillid))
            return _skillShooters[skillid];
        return null;
    }
}