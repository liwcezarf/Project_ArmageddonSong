﻿
using common;
using Luna3D;
using Vector3 = Luna3D.Vector3;

public class Hero : SkillContainer
{
    private Vector3 _dest;

    private Vector3 _forward;

    public Hero(Battle level, RoleData data) : base(level, data)
    {
    }

    public override void Die()
    {
        base.Die();

        RebornData data = new RebornData(globalID, 15);
        _battle.AddRebornData(data);
    }

    public override void Move(Vector3 dest, Vector3 forward)
    {
        _transform.SetPosition(dest);
        _transform.SetRotation(Quaternion.FromEulerAngles(forward.x, forward.y, forward.z));
    }
}