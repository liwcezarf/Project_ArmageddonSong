﻿
using System.Collections.Generic;
using Luna3D;
using Vector3 = Luna3D.Vector3;
using common;


/// <summary>
/// 角色基础
/// </summary>
public abstract class Character 
{

    public uint globalID = 0;


    private RoleData _originalData;

    // 角色属性
    protected CharacterAttribute _initAttribute;

    // 角色当前属性
    protected CharacterAttribute _attribute;

    // 角色配置
    private RoleConfig _config;

    protected RoleTransform _transform;

    // 导航代理
    protected LunaNavAgent _navAgent;

    protected Battle _battle;

    protected Character _target;

    public int RoleID { get { return _config.ID; } }

    public RoleSide side {  get { return _initAttribute.RoleSide; } }

    public RoleType type { get { return _initAttribute.RoleType; }}

    public int hp {get{  return _attribute.Hp;} }

    public Vector3 position{ get{ return _transform.GetPosition();  } }

    public Quaternion rotation{ get{ return _transform.GetRotation();} }

    public string name { set {  _transform.SetTranName(value); } get { return _transform.GetTranName();} }

    public bool alive { get { return _attribute.Hp > 0 ? true : false;  }}

    public bool movable {  get{ return _attribute.RoleType == RoleType.Tower ? false : true; } }


    public float sight {  get{   return _attribute.SightRange; } }

    public RoleData originalData { get{  return _originalData; } }

    public Vector3 forward { get{ return _transform.GetRotation() * Vector3.forward; } }


    public Character(Battle level, RoleData data)
    {
        _battle = level;
        _originalData = data;
        
        globalID = _originalData.UniID;

        _config = ConfigManager.instance.GetRoleConfig((int)_originalData.RoleID);
        _initAttribute = CharacterAttribute.GetAttribute(_originalData);
        _attribute = _initAttribute.Clone() as CharacterAttribute;

        _transform = new RoleTransform();
        _transform.SetPosition(ProtoHelper.PV2LV(_originalData.Pos));




        name = globalID + "_" + _initAttribute.RoleType;
    }

    public virtual void Update(float dt)
    {

    }

    public virtual void Move(Vector3 dest)
    {
    }

    public virtual void Move(Vector3 dest, Vector3 forward)
    {
    }

    public virtual void Idle()
    {         
    }


    private bool InAttackRange(Character target)
    {
        float distace = Vector3.Distance(target.position, position);
        if (distace <= _initAttribute.AttackRange)
            return true;

        return false;
    }

    /// <summary>
    /// 攻击
    /// </summary>
    public void TryAttack()
    {
        if (_navAgent.enabled)
        {
            _navAgent.enabled = false;
            BattleMsgSender.NotifyIdle(globalID, _battle.accounts);
        }

        SkillShooter shooter;
        if (CanShoot(out shooter))
        {
            Shoot(shooter, _target);
            BattleMsgSender.NotifyAttack(globalID, (uint)shooter.skillID, _target.globalID, position, _battle.accounts);
        }
    }

    /// <summary>
    /// 受击
    /// </summary>
    public void Damage(int hp)
    {
        if (_attribute.Hp <= 0)
            return;
        
        _attribute.Hp -= hp;

        if (_attribute.Hp <= 0)
        {
            _attribute.Hp = 0;
            Die();
        }

        name = globalID + "_" + _initAttribute.RoleType + "_" +  _attribute.Hp;

        BattleMsgSender.NotifyChangeHP(globalID, -hp, _battle.accounts);
    }

    /// <summary>
    /// 回复
    /// </summary>
    public void AddHp(int hp)
    {
        if (_attribute.Hp == _initAttribute.Hp)
            return;

        _attribute.Hp += hp;

        if (_attribute.Hp >= _initAttribute.Hp)
        {
            _attribute.Hp = _initAttribute.Hp;
        }

        name = globalID + "_" + _initAttribute.RoleType + "_" + _attribute.Hp;
        BattleMsgSender.NotifyChangeHP(globalID, hp, _battle.accounts);
    }


    public virtual void Die()
    {
        if (_navAgent.enabled)
            _navAgent.enabled = false;

        _navAgent.End();

        BattleMsgSender.NotifyDie(globalID, _battle.accounts);
    }


    /// <summary>
    /// 追击
    /// </summary>
    /// <param name="target"></param>
    public void Chase(Character target)
    {
        if (InAttackRange(target))
        {
            TryAttack();   
        }
        else
        {
            Move(target.position);            
        }
    }

    public void AddBullet(SkillBullet bullet)
    {
        _battle.AddBullet(bullet);
    }

    public Dictionary<uint, Character> GetAliveRoles(RoleSide side)
    {
        return _battle.GetAliveRoles(side);
    }

    public virtual Character FindNearestTypeEnemyInSight(RoleType type)
    {
        return null;
    }

    public virtual Character FindNearestTypeEnemy(RoleType type)
    {
        return null;
    }

    public virtual SkillShooter GetSkillShooter(int id)
    {
        return null;
    }

    public virtual void Shoot(SkillShooter shooter, Character ch) {}

    public virtual void TreatFriends(){ }

    public virtual bool CanShoot(out SkillShooter shooter)
    {
        shooter = null;
        return false;
    }
}
