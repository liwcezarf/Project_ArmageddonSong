﻿using System;
using System.Collections.Generic;
using System.Timers;


public class TimeHelper
{
    public static uint AppStartTime = 0;

    public static uint deltaTime;

    public static void Init()
    {
        AppStartTime = (uint)Environment.TickCount;
    }

    /// <summary>
    /// 获取当前时间
    /// </summary>
    /// <returns></returns>
    public static uint GetMSTime()
    {
        uint now = (uint)Environment.TickCount - AppStartTime;
        return now;
    }

    /// <summary>
    /// 获取时间增量
    /// </summary>
    /// <param name="oldTime"></param>
    /// <param name="newTime"></param>
    /// <returns></returns>
    public static uint GetMSTimeDiff(uint oldTime, uint newTime)
    {
        return newTime - oldTime;
    }
}
