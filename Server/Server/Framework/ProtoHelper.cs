﻿
using System;

public class ProtoHelper
{
    public static common.Vector3 Str2ProtoVec(string str)
    {
        string[] array = str.Split(',');
        common.Vector3 vec = new common.Vector3();
        vec.x = Convert.ToSingle(array[0]);
        vec.y = Convert.ToSingle(array[1]);
        vec.z = Convert.ToSingle(array[2]);

        return vec;
    }

    public static Luna3D.Vector3 Str2Vec(string str)
    {
        string[] array = str.Split(',');
        return new Luna3D.Vector3(Convert.ToSingle(array[0]), Convert.ToSingle(array[1]), Convert.ToSingle(array[2]));
    }

    public static Luna3D.Vector3 PV2LV(common.Vector3 protoVec)
    {
        return new Luna3D.Vector3(protoVec.x, protoVec.y, protoVec.z);
    }

    public static common.Vector3 LV2PV(Luna3D.Vector3 vec)
    {
        common.Vector3 ProtoVec = new common.Vector3();
        ProtoVec.x = vec.x;
        ProtoVec.y = vec.y;
        ProtoVec.z = vec.z;

        return ProtoVec;
    }
}
