﻿using System;
using System.Collections.Generic;
using System.Threading;

public class GameServer
{

    // 游戏循环线程
    private Thread _gameloopThread;

    private bool _gameloopRunning = false;

    private const int World_Sleep_Const = 50;

    private TimeHelper _timerHelper;

    private LogManager _log;


    public void Startup()
    {
        _log = new LogManager();
        _log.Init();

        ConfigManager.instance.Initialize(new ConfigParser());
        Console.WriteLine("ConfigManager Init");

        _timerHelper = new TimeHelper();
        TimeHelper.Init();
        LogManager.Log("Timer Init...");

        MysqlManager.instance.Connect();
        Console.WriteLine("Connect Mysql");

        // 服务器初始化
        NetworkManager ss = new NetworkManager(10000, new HandlerCenter());
        //NetworkManager.send = OnMsgSend;
        ss.Start(6650);
        Console.WriteLine("Start Listen");

        _gameloopRunning = true;

        _gameloopThread = new Thread(Run);
        _gameloopThread.Start();
        Console.WriteLine("Start GameLoop");
    }

    void OnMsgSend(UserToken token, int command, object msg)
    {
        Console.WriteLine("{0} 发送消息 {1}", token.accountid, (MsgID)command);
    }

    private void Tick(float dt)
    {
        BattleManager.instance.Tick(dt);
    }

    private void Run()
    {
        // 当前时间
        uint realCurrTime = 0;

        // 上一次更新开始的时间
        uint realPrevTime = TimeHelper.GetMSTime();

        // 上一次更新Sleep的时间
        uint prevSleepTime = 0;

        while (_gameloopRunning)
        {
            // 获取当前时间
            realCurrTime = TimeHelper.GetMSTime();

            // 更新增量时间
            uint diff = TimeHelper.GetMSTimeDiff(realPrevTime, realCurrTime);
            TimeHelper.deltaTime = diff;

            float framedt = diff / 1000f;

            Tick(framedt);


            realPrevTime = realCurrTime;

            if (diff <= World_Sleep_Const + prevSleepTime)
            {
                prevSleepTime = World_Sleep_Const + prevSleepTime - diff;
                Thread.Sleep((int)prevSleepTime);
            }
            else
            {
                prevSleepTime = 0;
            }
        }
    }
}