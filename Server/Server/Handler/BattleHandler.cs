﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using client;
using server;
using common;

public class BattleHandler : IMsgHandler
{
    public override void RegisterMsg(Dictionary<MsgID, Action<UserToken, SocketModel>> handlers)
    {
        handlers.Add(MsgID.ReqMove, OnRoleMove);
        handlers.Add(MsgID.ReqAttack, OnRoleAttack);
        handlers.Add(MsgID.ReqSyncHeroPos, OnReqSyncHeroPos);
    }


    /// <summary>
    /// 角色移动响应
    /// </summary>
    /// <param name="data"></param>
    /// <param name="user"></param>
    private void OnRoleMove(UserToken token, SocketModel model)
    {
        // 解析消息
        ClientMsg req = SerializeUtil.Deserialize<ClientMsg>(model.message);

        Account account = CacheManager.instance.GetAccount(token.accountid);
        Battle level = BattleManager.instance.GetLevel(account.globalLevelID);
        Character role = level.GetRole(account.globalRoleID);
        role.Move(ProtoHelper.PV2LV(req.RoleMove.dest));

        BattleMsgSender.NotifyMoveToOther(account.globalRoleID, account.uid, req.RoleMove.dest, level.accounts);
    }

    /// <summary>
    /// 请求同步英雄坐标
    /// </summary>
    /// <param name="token"></param>
    /// <param name="model"></param>
    private void OnReqSyncHeroPos(UserToken token, SocketModel model)
    {
        ClientMsg req = SerializeUtil.Deserialize<ClientMsg>(model.message);

        Account account = CacheManager.instance.GetAccount(token.accountid);
        Battle level = BattleManager.instance.GetLevel(account.globalLevelID);
        Character role = level.GetRole(account.globalRoleID);
        role.Move(ProtoHelper.PV2LV(req.syncHeroPos.dest), ProtoHelper.PV2LV(req.syncHeroPos.forward));

        BattleMsgSender.NotifyMoveToOther(account.globalRoleID, account.uid, req.syncHeroPos.dest, level.accounts);
    }

    /// <summary>
    /// 角色攻击
    /// </summary>
    /// <param name="data"></param>
    /// <param name="user"></param>
    private void OnRoleAttack(UserToken token, SocketModel model)
    {
        ClientMsg req = SerializeUtil.Deserialize<ClientMsg>(model.message);

        Account account = CacheManager.instance.GetAccount(token.accountid);
        Battle level = BattleManager.instance.GetLevel(account.globalLevelID);
        Character role = level.GetRole(account.globalRoleID);
        Character target = level.GetRole(req.RoleAttack.targetID);

        SkillShooter shooter = role.GetSkillShooter((int)req.RoleAttack.skillID);

        if (shooter.CDComplete())
        {
            // 服务端角色施法
            role.Shoot(shooter, target);

            // 通知战斗中所有客户端这个角色施法
            ServerMsg notify = new ServerMsg();
            notify.RoleAttack = new server.NotifyRoleAttack();
            notify.RoleAttack.globalRoleID = role.globalID;
            notify.RoleAttack.pos = ProtoHelper.LV2PV(role.position);
            notify.RoleAttack.skillID = req.RoleAttack.skillID;
            notify.RoleAttack.targetid = req.RoleAttack.targetID;

            MsgSender.BroadCast(level.accounts, MsgID.NotifyRoleAttack, notify);
        }
    }
}