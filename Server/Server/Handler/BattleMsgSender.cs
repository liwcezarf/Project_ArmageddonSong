﻿using server;
using Luna3D;
using System.Collections.Generic;

public class BattleMsgSender
{

    /// <summary>
    /// 通知角色死亡
    /// </summary>
    /// <param name="globalRoleID">角色全局ID</param>
    /// <param name="accounts">要通知的所有账号</param>
    public static void NotifyDie(uint globalRoleID, Dictionary<string, Account> accounts)
    {
        ServerMsg notify = new ServerMsg();
        notify.RoleDie = new NotifyRoleDie();
        notify.RoleDie.globalRoleID = globalRoleID;
        MsgSender.BroadCast(accounts, MsgID.NotifyRoleDie, notify);
    }

    /// <summary>
    /// 通知移动
    /// </summary>
    /// <param name="globalRoleID">角色全局ID</param>
    /// <param name="position">目标点</param>
    /// <param name="accounts">所有账号</param>
    public static void NotifyMove(uint globalRoleID, Vector3 position, Dictionary<string, Account> accounts)
    {
        ServerMsg notify = new ServerMsg();
        notify.RoleMove = new NotifyRoleMove();
        notify.RoleMove.globalRoleID = globalRoleID;
        notify.RoleMove.position = ProtoHelper.LV2PV(position);

        MsgSender.BroadCast(accounts, MsgID.NotifyRoleMove, notify);
    }

    /// <summary>
    /// 通知移动
    /// </summary>
    /// <param name="globalRoleID">角色全局ID</param>
    /// <param name="position">目标点</param>
    /// <param name="accounts">所有账号</param>
    public static void NotifyMoveToOther(uint globalRoleID, uint accountid, common.Vector3 position, Dictionary<string, Account> accounts)
    {
        ServerMsg notify = new ServerMsg();
        notify.RoleMove = new NotifyRoleMove();
        notify.RoleMove.globalRoleID = globalRoleID;
        notify.RoleMove.position = position;

        MsgSender.BroadCastToOther(accounts, accountid, MsgID.NotifyRoleMove, notify);
    }

    /// <summary>
    /// 通知待机，停止移动
    /// </summary>
    /// <param name="globalRoleID"></param>
    /// <param name="accounts"></param>
    public static void NotifyIdle(uint globalRoleID, Dictionary<string, Account> accounts)
    {
        ServerMsg notify = new ServerMsg();
        notify.RoleIdle = new NotifyRoleIdle();
        notify.RoleIdle.globalRoleID = globalRoleID;
        MsgSender.BroadCast(accounts, MsgID.NotifyRoleIdle, notify);

    }

    /// <summary>
    /// 通知攻击事件
    /// </summary>
    /// <param name="globalRoleID">攻击者的全局ID</param>
    /// <param name="skillID">技能ID</param>
    /// <param name="targetid">目标的全局ID</param>
    /// <param name="position">坐标</param>
    /// <param name="accounts">所有账号</param>
    public static void NotifyAttack(uint globalRoleID, uint skillID, uint targetid, Vector3 position, Dictionary<string, Account> accounts)
    {
        ServerMsg notify = new ServerMsg();
        notify.RoleAttack = new NotifyRoleAttack();
        notify.RoleAttack.globalRoleID = globalRoleID;
        notify.RoleAttack.skillID = skillID;
        notify.RoleAttack.pos = ProtoHelper.LV2PV(position);
        notify.RoleAttack.targetid = targetid;
        MsgSender.BroadCast(accounts, MsgID.NotifyRoleAttack, notify);
    }

    /// <summary>
    /// 通知血量改变
    /// </summary>
    /// <param name="targetid">目标的全局ID</param>
    /// <param name="hp">血量</param>
    /// <param name="accounts">所有账号</param>
    public static void NotifyChangeHP(uint targetid, int hp, Dictionary<string, Account> accounts)
    {
        ServerMsg notify = new ServerMsg();
        notify.HPChange = new NotifyHPChange();
        notify.HPChange.targetid = targetid;
        notify.HPChange.hp = hp;

        MsgSender.BroadCast(accounts, MsgID.NotifyHPChange, notify);
    }
}