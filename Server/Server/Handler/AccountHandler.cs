﻿using System;
using client;
using server;
using System.Collections.Generic;

public class AccountHandler : IMsgHandler
{
    int i = 0;
    public override void RegisterMsg(Dictionary<MsgID, Action<UserToken, SocketModel>> handlers)
    {
        handlers.Add(MsgID.ReqRegist, OnRegist);
        handlers.Add(MsgID.ReqLogin, OnLogin);
    }
    /// <summary>
    /// 注册
    /// </summary>
    /// <param name="data"></param>
    /// <param name="user"></param>
    private  void OnRegist(UserToken token, SocketModel model)
    {
        Console.WriteLine("已注册：" + ++i);

        //ClientMsg req = SerializeUtil.Deserialize<ClientMsg>(model.message);


        //// 从数据库里查询有没有这个账号
        //string sql = string.Format("select * from account where account = '{0}'", req.Register.Account);
        //List<AccountData> accs = MysqlManager.instance.ExecQuery<AccountData>(sql);


        // 创建注册消息
        ServerMsg resp = new ServerMsg();
        resp.Register = new server.RespRegister();

        //if (accs.Count > 0)
        //{
        //    resp.ErrorCode = (uint)ErrorCode.AccountNameRepeat;
        //}
        //else
        //{
        //    resp.Register.Account = req.Register.Account;
        //    resp.Register.Password = req.Register.Password;

        //    // 往数据库里插入一个账号
        //    sql = string.Format("insert into account(account, pwd) values('{0}','{1}')", req.Register.Account, req.Register.Password);
        //    MysqlManager.instance.ExecNonQuery(sql);

        //}

        NetworkManager.Send(token, (int)MsgID.RespRegist, resp);

    }

    /// <summary>
    /// 登录
    /// </summary>
    /// <param name="data"></param>
    /// <param name="user"></param>
    private  void OnLogin(UserToken token, SocketModel model)
    {
        ClientMsg req = SerializeUtil.Deserialize<ClientMsg>(model.message);
        ServerMsg resp = new ServerMsg();
        resp.Login = new server.RespLogin();

        // 从数据库中查询是否包含该账号
        string sql = string.Format("select * from account where account = '{0}'", req.Login.AccountName);
        List<AccountData> accDatas = MysqlManager.instance.ExecQuery<AccountData>(sql);

        if (accDatas.Count <= 0)
        {
            resp.ErrorCode = (uint)ErrorCode.AccountNotRegister;
        }
        else
        {
            AccountData acc = accDatas[0];

            if (CacheManager.instance.IsAccountOnline((uint)acc.id))     // 账号已经在线，就不让再登录了
            {
                resp.ErrorCode =(uint)ErrorCode.AccountHasLogin;
            }
            else
            {
                if (acc.pwd == req.Login.Password)
                {
                    CacheManager.instance.AccountOnline((uint)acc.id, req.Login.AccountName, token);

                    resp.Login.Account = req.Login.AccountName;
                    resp.Login.Password = req.Login.Password;
                    token.accountid = (uint)acc.id;
                }
                else
                {
                    resp.ErrorCode = (uint)ErrorCode.PasswordError;
                }
            }
        }
        NetworkManager.Send(token, (int)MsgID.RespLogin, resp);
    }
}