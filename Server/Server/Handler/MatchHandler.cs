﻿using System;
using System.Collections.Generic;
using client;
using server;
using common;


public class MatchHandler : IMsgHandler
{
  
    public override void RegisterMsg(Dictionary<MsgID, Action<UserToken, SocketModel>> handlers)
    {
        handlers.Add(MsgID.ReqActualCombat, OnActualComatat);
        handlers.Add(MsgID.ReqCancelMatch, OnCancelMatch);
        handlers.Add(MsgID.ReqReady, OnReqReady);
        handlers.Add(MsgID.ReqExitLevel, OnExitLevel);
    }


    // 实战模式应答
    private  void OnActualComatat(UserToken token, SocketModel model)
    {
        ClientMsg req = SerializeUtil.Deserialize<ClientMsg>(model.message);

        // 获取未满的房间
        MatchData match = CacheManager.instance.GetNotFullMatch(LevelType.ActualCombat, req.ActualCombat.PlayerNumber);

        // 所有房间已满就新创建一个房间
        if (match == null)
        {
            match = CacheManager.instance.CreateMatch(LevelType.ActualCombat, req.ActualCombat.PlayerNumber);
        }

        // 往房间中添加一个账号
        Account account = CacheManager.instance.GetAccount(token.accountid);
        match.AddAccount(account);

        // 如果房间已满，就向房间中的所有账号客户端发送选择英雄消息
        if (match.full)
        {
            // 创建英雄选择消息实例
            ServerMsg notify = new ServerMsg();
            notify.ChooseHero = new NotifyChooseHero();
            notify.ChooseHero.LevelType = LevelType.ActualCombat;

            // 往消息中填充备选英雄
            Dictionary<int, RoleConfig> _roleCfgs = ConfigManager.instance.GetRolesByType(RoleType.Hero);
            foreach (RoleConfig cfg in _roleCfgs.Values)
            {
                if (!notify.ChooseHero.OptionalHeros.Contains((uint)cfg.ID))
                    notify.ChooseHero.OptionalHeros.Add((uint)cfg.ID);
            }

            // 往消息中填充匹配双方所有账号匹配信息
            foreach (Account acc in match.accounts.Values)
            {
                MatchAccountInfo accInfo = new MatchAccountInfo();
                // 房间中账号的索引
                accInfo.Index = (uint)acc.matchIndex;
                // 账号名
                accInfo.AccountName = acc.accountName;
                // 账号所在阵营
                accInfo.RoleSide = acc.roleSide;
                notify.ChooseHero.Accounts.Add(accInfo);
            }

            MsgSender.BroadCast(match.accounts, MsgID.NotifyChooseHero, notify);
        }
        else
        {
            // 如果房间未满，就向请求的客户端发送实战应答，让他等待
            ServerMsg resp = new ServerMsg();
            resp.ActualCombat = new RespActualCombat();
            resp.ActualCombat.PlayerNumber = req.ActualCombat.PlayerNumber;

            NetworkManager.Send(token, (int)MsgID.RespActualCombat, resp);
        }
    }

    // 取消匹配
    private  void OnCancelMatch(UserToken token, SocketModel model)
    {
        ClientMsg req = SerializeUtil.Deserialize<ClientMsg>(model.message);

        // 从账号容器中查询账号
        Account account = CacheManager.instance.GetAccount(token.accountid);

        // 从匹配容器中查询匹配
        MatchData match = CacheManager.instance.GetMatch(account.globalMatchID);

        // 从匹配容器中移除该账号
        if (match != null)
        { 
            match.RemoveAccount(account);

            // 往客户端发送取消匹配应答
            ServerMsg resp = new ServerMsg();
            resp.CancelMatch = new RespCancelMatch();
            NetworkManager.Send(token, (int)MsgID.RespCancelMatch, resp);
        }
    }

    /// <summary>
    /// 准备进入战斗的消息响应
    /// </summary>
    /// <param name="data"></param>
    /// <param name="user"></param>
    private  void OnReqReady(UserToken token, SocketModel model)
    {
        ClientMsg req = SerializeUtil.Deserialize<ClientMsg>(model.message);

        // 获取账号信息,设置账号选择的英雄ID
        Account account = CacheManager.instance.GetAccount(token.accountid);
        account.heroID = req.Ready.HeroID;
        account.ready = true;

        MatchData match = CacheManager.instance.GetMatch(account.globalMatchID);


        // 通知匹配中所有玩家某人已经准备好
        ServerMsg ready = new ServerMsg();
        ready.Ready = new NotifyReady();
        ready.Ready.side = account.roleSide;
        ready.Ready.index = (uint)account.matchIndex;
        ready.Ready.heroid = account.heroID;
        ready.Ready.accountName = account.accountName;
        MsgSender.BroadCast(match.accounts, MsgID.NotifyReady, ready);

        // 所有玩家都已准备好
        if (match.ready)
        {
            // 创建关卡
            Battle battle = BattleManager.instance.Create(match.levelType, (int)match.accountNumLimit);

            // 往关卡中填充玩家账号数据, 设置战斗中所有账号的全局关卡ID, 服务端玩家出生
            foreach (Account acc in match.accounts.Values)
            {
                acc.globalLevelID = battle.globalID;

                BornPointCfg bornPointCfg = ConfigManager.instance.GetBornPoint(battle.config.ID, RoleType.Hero, acc.roleSide);
                string pos = bornPointCfg.InitPosition;
                string rot = bornPointCfg.InitRotation;
                RoleData heroData = battle.HeroBorn((int)acc.heroID, acc.roleSide, pos, rot, acc.accountName);
                acc.globalRoleID = heroData.UniID;

                battle.AddAccount(acc);
            }

            // 移除匹配
            CacheManager.instance.RemoveMatch(match);

            // 通知所有玩家进入关卡
            ServerMsg notify = new ServerMsg();
            notify.EnterLevel = new NotifyEnterLevel();
            notify.EnterLevel.mapid = (uint)battle.config.ID;

            // 所有角色数据
            List<RoleData> roles = battle.GetAllRoleDatas();
            foreach (RoleData hero in roles)
            {
                notify.EnterLevel.RoleList.Add(hero);
            }

         
            MsgSender.BroadCast(match.accounts, MsgID.NotifyEnterLevel, notify);
        }
    }

    private void OnExitLevel(UserToken token, SocketModel model)
    {
        ClientMsg req = SerializeUtil.Deserialize<ClientMsg>(model.message);
    }
}