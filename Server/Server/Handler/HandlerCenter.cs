﻿using System;
using System.Collections.Generic;


public abstract class IMsgHandler
{
    public abstract void RegisterMsg(Dictionary<MsgID, Action<UserToken, SocketModel>> handlers);


}

public class HandlerCenter :  IHandlerCenter
{
    private Dictionary<MsgID, Action<UserToken, SocketModel>> _handlers = new Dictionary<MsgID, Action<UserToken, SocketModel>>();

    private AccountHandler _loginHandler = new AccountHandler();

    private MatchHandler _worldHandler = new MatchHandler();

    private BattleHandler _levelHandler = new BattleHandler();
    public void Initialize()
    {
        _loginHandler.RegisterMsg(_handlers);
        _worldHandler.RegisterMsg(_handlers);
        _levelHandler.RegisterMsg(_handlers);
    }

    /// <summary>
    /// 客户端连接到服务器
    /// </summary>
    /// <param name="token"></param>
    public void ClientConnect(UserToken token)
    {
        Console.WriteLine(string.Format("{0} Connnect...", token.address.ToString()));
    }

    /// <summary>
    /// 客户端断开连接
    /// </summary>
    /// <param name="token"></param>
    /// <param name="error"></param>
    public void ClientClose(UserToken token, string error)
    {
        Console.WriteLine(string.Format("{0} Disconnect...", token.address.ToString()));

        Account acc = CacheManager.instance.GetAccount(token.accountid);
        if (acc == null)
        {
            LogManager.Log("Cannot find account:" + token.accountid);
            return;
        }
        else
        {
            CacheManager.instance.RemoveAccount(token.accountid);
        }

        // 终止当前匹配
        MatchData match = CacheManager.instance.GetMatch(acc.globalMatchID);
        if (match != null)
        {
            CacheManager.instance.RemoveMatch(match);

            server.ServerMsg resp = new server.ServerMsg();
            resp.CancelMatch = new server.RespCancelMatch();
            MsgSender.BroadCastToOther(match.accounts, acc.client.accountid, MsgID.RespCancelMatch, resp);
        }
    }

    /// <summary>
    /// 服务端在收到客户端的消息之后要执行的方法
    /// </summary>
    /// <param name="token"></param>
    /// <param name="message"></param>
    public void MessageReceive(UserToken token, object message)
    {
        SocketModel model = message as SocketModel;

        //Console.WriteLine(token.accountid +", "+ (MsgID)model.command);

        Action<UserToken, SocketModel> handler = _handlers[(MsgID)model.command];
        handler(token, model);
    }
}